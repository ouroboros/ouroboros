from extendedInts import *

######################################################################
#                                                                    #
#             Linked Lists                                           #
#             @author: T. Carle                                      #
#                                                                    #
######################################################################



class Ll(): # can be replaced by a python list for small sets
    """
    Simple linked list
    """
    def __init__(self, elt):
        self.val=elt
        self.nextVal=None
        self.predVal=None

    def __str__(self):
        if self.val == None:
            return ""
        return str(self.val)#+ " pred = "+str(self.predVal) + " next= "+str(self.nextVal)

    def __getattr__(self, n):
        return getattr(self.getVal(), n)
    
    def buildNew(self, elt):
        return Ll(elt)
    
    def getVal(self):
        return self.val

    def getNext(self):
        return self.nextVal

    def setVal(self, val):
        assert self.val==None
        self.val=val

    def setNext(self, nextVal):
        self.nextVal=nextVal

    def setPred(self, predVal):
        self.predVal=predVal
        
    def getPred(self):
        return self.predVal

    def insertAfter(self, prevNode):
        """
        inserts prevNode (treated as a lonely node) just before the current node in the Ll
        """
        
        prevNode.setNext(self)
        prevNode.setPred(self.predVal)
        if self.predVal != None:
            self.predVal.setNext(prevNode)
        self.prev=prevNode

    def insertBefore(self, nextNode):
        """
        inserts nextNode (treated as a lonely node) just after the current node in the Ll
        """
        nextNode.setPred(self)
        nextNode.setNext(self.nextVal)
        if self.nextVal != None:
            self.nextVal.setPred(nextNode)
        self.nextVal=nextNode


    def insert(self, tSlot): # This function is here to comply with the future interface for schedule representations
        tempNode=self
        tSlotNode=self.buildNew(tSlot)
        while(tempNode.getNext()!=None and tempNode.getNext().getOrder() < tSlotNode.getOrder()):
            tempNode=tempNode.getNext()
        tempNode.insertBefore(tSlotNode)


    def getOrder(self):
        """
        Returns an integer that is representative of the sorting order of the list
        """
        if self.getVal()==None:
            return -1 # We consider we will never have elements with negative value (e.g. dates)
        else:
            return self.getVal().getOrder()
        
    def remove(self):
        if self.predVal == None:
            if self.nextVal == None:
                #print("Removing only element of the list")
                return (self.buildNew(None), True)
            else:
                self.nextVal.setPred(None)
                return (self.nextVal, True) # Removing the first element of the list
        else:
            if self.nextVal != None:
                self.nextVal.setPred(self.predVal)
            self.predVal.setNext(self.nextVal)
            return (self.nextVal, False)
        
        
    def toList(self):
        l=[self.val]
        temp=self
        while temp.getNext()!= None:
            temp=temp.getNext()
            l.append(temp.val)
        return l

    
        

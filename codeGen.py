#from scheduler import *
from archInterference import *
from variables import *
from tasks import *
from functions import *
from synchronousApps import *

######################################################################
#                                                                    #
#             Ouroboros module for code generation                   #
#             @author: T. Carle                                      #
#                                                                    #
######################################################################

# TODO: Need a code generator for simple multicore architectures AND a code generator for architectures with DMA

class Phi(Variable):
    """
    A class for the on-the-fly generation of phi functions to join
    conditional (synchronous) predecessors
    """
    #pred must be a synchronous predecessor
    def __init__(self, idx, pred):
        var = pred.getVar()
        varType = var.getVarType()
        Variable.__init__(self, idx, "phi"+str(idx), varType)

    def declare(self):
        return self.getVarTypeName()+ " "+ self.getName() + ";\n"

class SyncInfo():
    """
    A class to gather synchronization information for a given slot. This one is basic, for TT synchronization. Must be inherited for other synchronization methods
    """
    def __init__(self, job):
        self.release = job.getReleaseDate()
        self.ready = job.getReadyDateMulticore()
        self.start = job.getStartDate()
        
    def genSyncMacro():
        """
        Virtual: to be overloaded for each kind of architecture/synchronization method
        """
        return "#define SYNC(n) \n\n" 
        
    def genSync(self):
        """
        TT implementation: tasks are synchronized at the date chosen in
        the schedule. To be overloaded for other synchronization
        methods

        """
        #return "SYNC("+str(max(self.release, self.ready))+");\n"
        return "SYNC("+str(self.start)+");\n"
    
    def necessary(self):
        """
        Is a synchronization necessary ? In a full TT implementation, it
        is always necessary (reduces jitter). To be overloaded for
        other synchronization methods

        """
        #return self.release>0 or self.ready>0
        return True
    
#####################################################################################################
# Generic code generator
#####################################################################################################
class CodeGen():
    """
    Description of a generic code generator for tasks with ports and clocks. Can be inherited to refine the model.
    """
    def __init__(self, archi, tasks, functions, variables):
        self.archi = archi 
        self.tasks = tasks # The description of the tasks, which contains information on the ports and dependencies
        self.functions = functions
        self.variables = variables

    def extractScheme(self, proc):
        """
        Extract the execution scheme (sequence of function calls) for a
        proc. We get the list of slots for the proc, and filter out
        the ones corresponding to preempted tasks being resumed.

        """
        launched = {} # Maps tasks to a list of jobs that are already launched in the scheme (in order to know which functions not to call again in the presence of preemptions)
        sched = proc.getSchedule()

        #First node of LL is a sentinel
        if sched != None:
            sched = sched.getNext()
        schedList = sched.toList()
        scheme = []
        for s in schedList:
            if s.getTaskIdx() not in launched:
                launched[s.getTaskIdx()] = [s.getJob().getIdx()]
                scheme.append(s)
            else:
                al = launched[s.getTaskIdx()]
                if s.getJob().getIdx() not in al:
                    launched[s.getTaskIdx()].append(s.getJob().getIdx())
                    scheme.append(s)
        return scheme

    def syncScheme(self, slot):
        """
        In order to know whether or not a synchronization is needed, we
        need to locate key elements: start date, end of
        predecessors. The synchronization in itself depends on
        implementation choices (TT, ET, choice of locks etc.) which
        must be tackled in another function. This function only
        retrieves generic information to generate syncs. This version
        is very simple, and must be overloaded for more complex
        architectures (e.g. with DMA -- account for data transfers)
        and task models (using heritage on SyncInfo).

        """
        job = slot.getJob()
        return SyncInfo(job) 

    def generateCoreCode(self, core):
        """
        Generate code for a core. When using clocks, requires the base clock from which all
        other clocks of the core will be derived

        """
        # Counter for phi-functions
        phi = 0
        
        out = self.generateMacros()
        out += self.generateMain(core)
        
        out += "\twhile(1){\n"
        l = self.extractScheme(core)
        for s in l:
            si = self.syncScheme(s)
            if si.necessary():
                out += "\t\t" + si.genSync()

            #retrive corresponding task
            task = self.tasks[s.getJob().getTaskIdx()]
           
            #retrieve calling parameters    
            op = task.getOPorts()
            ip = task.getIPorts()
            
            #generate execution guard
            out+=self.generateGuard(task)
            
            #input vars, output vars
            iv, ov = [], []
            for p in ip:
                # p is input port. We check if there are more than 1
                # possible (conditional) variables for this input port
                v, phi2, out2 = self.manageFunctionParameters(p, s)
                iv.append(v)
                phi += phi2
                out += out2 
            for p in op:
                ov.append(p.getVariable().getName())

            # generate call to function
            # if slot is not for a special function (e.g. DMA write)
            if not s.getSpecialFun():
                out += "\t\t" + self.generateRegularFunCall(task, iv, ov)
                
            else: #special function: for now DMA only
                out += self.generateSpecialFunCall(s, iv, ov)

            out += self.generateEndGuard(task)
                
        out += "\t}//End while(1)\n"
        out += self.generateEnd()
        return out


    def manageFunctionParameters(self, p, s):
        """
        Retrieve function input parameters. To be overloaded to
        generate phi functions if inputs are conditional. p is an input port. s is the corresponding slot.
        """ 
        return p.getPreds()[0].getVarName(), 0, ""
    
    def generateRegularFunCall(self, task, iv, ov):
        """
        Generation of a user-defined function call
        """
        out = ""
        out +=  self.functions[task.getFunIdx()].getName() + "("
        for i in range(len(iv)-1):
            out += iv[i] + ", "
        if len(iv) > 0:
            out += iv[-1]
            if len(ov) > 0:
                out += ","
        for i in range(len(ov)-1):
            out += "&"+ov[i] + ", "
        if len(ov)>0:
            out+="&"+ov[-1]
        out += ");\n"
        return out

    def generateSpecialFunCall(self, slot, iv, ov):
        """
        Generation of a special function call (e.g. DMA write)
        """
        return ""
        
    def generateMain(self, core):
        """
        Generic main function to be specialized for various architectures via heritage
        """
        return "int main(void){\n" # For now, only one main #+"_core"+ str(core.getIdx())+

    def generateEnd(self):
        """
        End of main function
        """
        return "}"

    def generateMacros(self):
        """
        Generates the macros for the main program. At least it generates a
        macro for the synchronization method

        """
        return SyncInfo.genSyncMacro()
    
    def generateGuard(self, task):
        """
        Virtual: when there are conditions/clocks: generates the code for the guard
        """
        return ""
    def generateEndGuard(self, task):
        """
        Virtual: when there are conditions/clocks: generates the closing } for the guard code block
        """
        return ""
    
    def generateVariableDeclarations(self):
        """
        Simple, for single core. Must be overloaded for more complex targets
        """
        out = ""
        for v in self.variables:
            assert v.getVarType() != None, "To generate code, all variables must have a type"
            out += v.getVarTypeName() + " " + v.getName() + ";\n"
        return out
            
class SyncCodeGen(CodeGen):
    """
    Code generator which handles synchronous clocks (TODO: handle phy functions for conditional inputs)
    """
    def __init__(self, archi, tasks, functions, variables):
        CodeGen.__init__(self, archi, tasks, functions, variables)
        
    def generateGuard(self, task):
        # generate guard
        clk = task.getClock()
        if not clk.equivalent(Clock()):
            return "\t\tif("+clk.printClock()+"){\n\t"
        return ""

    def generateEndGuard(self, task):
        clk = task.getClock()
        if not clk.equivalent(Clock()):
            return "\t\t}\n"
        return ""
    

    def manageFunctionParameters(self, p, s):
        """
        Adding phi function generation for conditional inputs
        """
        var = ""
        out = ""
        phi = 0
        if len(p.getPreds())>1 and not s.getSpecialFun():
            # There are conditional variables for the input:
            # we must add a phi-function and a new local
            # variable
            v = Phi(phi, p.getPreds()[0])
            var = v.getName()
            out += "\t\t"+v.declare()
            phi +=1
            #iv.append(var.getName())
            for pred in p.getPreds():    
                out+= "\t\tif("+ pred.getClock().printClock()+")\n"
                out+= "\t\t\t"+v.getName()+" = "+ pred.getVarName()+";\n"
        else:# Only one input variable, no need for a phi-function        
            var, phi, out = CodeGen.manageFunctionParameters(self, p, s)
        return var, phi, out

#####################################################################################################
# STM32 code generator
#####################################################################################################


class STM32SyncInfo(SyncInfo):
    def __init__(self, job):
        SyncInfo.__init__(self, job)

    def genSyncMacro():
        """
        Overload for STM32 with simple busy loop wait
        """
        return "#define SYNC(n) for (i = 0; i < 6000000; i++) __asm__(\"nop\");\n\n" #For STM32 

        
    
class STM32CodeGen(SyncCodeGen):
    
    def generateMain(self, core):
        out = SyncCodeGen.generateMain(self, core)
        out += self.generateVariableDeclarations()
        return out

    def generateMacros(self):

        out = "#include <libopencm3/stm32/rcc.h>\n"
        out+= "#include <libopencm3/stm32/gpio.h>\n"
        out+= "#include <itm.h>\n"
        out+= "#include \"IO_F4_Discovery.h\"\n"
        out+= "#include \"libs.h\"\n\n"
        out += STM32SyncInfo.genSyncMacro()
        return out
    
    def syncScheme(self, slot):
        """
        In order to know whether or not a synchronization is needed, we
        need to locate key elements: start date, end of
        predecessors. The synchronization in itself depends on
        implementation choices (TT, ET, choice of locks etc.) which
        must be tackled in another function. This function only
        retrieves generic information to generate syncs. This version
        is very simple, and must be overloaded for more complex
        architectures (e.g. with DMA -- account for data transfers)
        and task models (using heritage on SyncInfo).

        """
        job = slot.getJob()
        return STM32SyncInfo(job) 


    
# vt = VariableType(0, "int")
# v0 = IntVar(0,"v0", vt)
# v1 = Variable(1,"v1", vt)
# #v1 = IntVar(1,"v1", vt)
# v2 = Variable(2,"v2", vt)
# variables= [v0,v1,v2]

# f0 = MonolithicFunction(0, "f0", [3,2])
# f1 = MonolithicFunction(1, "f1", [2,1])
# f2 = MonolithicFunction(2, "f2", [1,2])
# functions = [f0,f1,f2]

# op0 = OPort(v0)
# op1 = OPort(v1)
# op2 = OPort(v2)

# m = SynchronousSemantics()
# Sn, Sd, Sf = m.SynchronousNodeTypes(DataflowNode)


# ieq0 = v0 <= 1
# ieq1 = v0 > 1

# c0 = Clock()
# c1 = c0 < ieq0
# c2 = c0 < ieq1
# c3 = c1 + c2


# sn0 = Sf("sn0", 0, 0, [], c0, [op0, op1])

# ip0 = IPort([SynchPred(0, op0, c1), SynchPred(0, op1, c2) ])

# sn1 = Sf("sn1", 1, 1,[ip0], c0, [op2])
# #sn2 = Sf("sn2",2,2,[], c0, [])
# tasks = [sn0, sn1]

# p0 = Processor(0,0)
# arch = Architecture([p0])

# Ss = m.synchronousScheduler(DataflowMonolithic)

# sched = Ss(tasks, arch, functions)
# sched.schedule()

# cg = STM32CodeGen(arch, tasks, functions, variables)

# print(cg.generateCoreCode(p0))

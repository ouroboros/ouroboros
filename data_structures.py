from extendedInts import *
from LL import *
from schedule import *

######################################################################
#                                                                    #
#             Data structures for Ouroboros                          #
#             @author: T. Carle                                      #
#                                                                    #
######################################################################


class RemainLists():
    """A data structure to simplify the management of schedule
    recomputation in the presence of interference. The structure
    includes a list of linked lists (one for each processing resource). Each
    linked list is composed of time slots that were previously scheduled on
    the corresponding processing resource, sorted by starting
    date. The data structure also has a counter to access quickly the
    number of remaining slots to be rescheduled. 
    """
    def __init__(self, lOfll):
        self.remainders = lOfll

        i=0
        for subl in self.remainders:
            sIter = subl
            while sIter != None:
                i+=1
                sIter=sIter.getNext()
        self.count = i

    def getCount(self):
        return self.count

    def isEmpty(self):
        return self.count == 0
    
    def getMin(self, procs):# We need the current state of the processing resources to determine their availability dates
        """
        Returns the slot corresponding to the job ready to be scheduled the earliest (preds + dispo sur proc), and removes it from its list. Also reduces the count of number of slots
        """
        assert self.count > 0, "No more slots to re-schedule"
        currentDate = ExtendedInt(1,True) # We start with infty 
        currentListIdx = None
        i=0
        while i < len(self.remainders):
            slot=self.remainders[i]
            if slot!=None:
                # We want to get the candidate with the minimum
                # available date (inputs ready + processing resource
                # available)
                temp= max(slot.getVal().getJob().getReadyDateMulticore(), procs[i].getEndDate())
                if temp < currentDate:
                    currentDate = temp
                    currentListIdx = i
            i+=1
        self.count -= 1
        res = self.remainders[currentListIdx]
        self.remainders[currentListIdx]=res.getNext()
        return res, currentDate

    def toList(self):
        res = []
        for ll in self.remainders:
            llBis = ll
            while llBis!= None:
                res.append(llBis.getVal())
                llBis = llBis.getNext()
        return res

import argparse
import json
import os
import os.path
import sys
import xml.etree.ElementTree

from codeGen import *

######################################################################
#                                                                    #
#             Ouroboros module for Lightweight                       #
#         Integration Scheduler and Compiler                         #
#         Ordonnanceur Léger d'Intégration (OLI v.1a)                #
#             @author: T. Carle                                      #
#                                                                    #
######################################################################



class SynchronousApploader():
    def __init__(self, path, name):
        self.name = name
        self.app = json.load(open(path))
        self.varTypes = []
        self.variables = []
        self.clocks = []
        self.clockMap = {}
        self.tasks = []
        self.functions = []
        self.archi = None
        m = SynchronousSemantics()
        Sn, Sd, Sf = m.SynchronousNodeTypes(DataflowNode)
        self.delayType = Sd
        self.funType = Sf
        self.schedType = m.synchronousScheduler(DataflowMonolithic)
        self.scheduler = None

        
    def getType(self, tIdx):
        return self.varTypes[tIdx]

    def gatherVarTypes(self):
        print("Gathering variable types...")
        for vt in self.app["types"]:
            self.varTypes.append(VariableType(vt.get("idx"), vt.get("name")))
        print("Done")
        
    def gatherVariables(self):
        print("Gathering variables...")
        for v in self.app["variables"]:
            if v["kind"]=="Variable":
                self.variables.append(Variable(v["idx"], v["name"], self.getType(v["typeIdx"])))
            elif v["kind"]=="IntVar":
                self.variables.append(IntVar(v["idx"], v["name"], self.getType(v["typeIdx"])))
            elif v["kind"]=="BoolVar":
                self.variables.append(BoolVar(v["idx"], v["name"], self.getType(v["typeIdx"])))
        print("Done")
        
    def gatherClocks(self):
        print("Gathering clocks...")
        # We need to execute the declaration of the variables, so the
        # variable names are recognized when creating the clocks
        for v in self.variables:
            exec("global "+ v.getName()+"; "+ v.getName()+ "="+ self.name+ ".variables["+ str(v.getIdx())+"]")

        # For each clock, we must execute the declaration of the clock
        # (so that its name is recognized if the clock is used in
        # other clocks declarations, and append it to the list of
        # clocks
        for c in self.app["clocks"]:
            exec("global "+ c["name"]+"; "+ c["name"]  +"=" + c["expr"] + "; "+ self.name + ".clocks.append("+c["name"]+ ")")
            self.clockMap[c["idx"]] = self.clocks[-1]
        print("Done")    
        
    def gatherTasks(self):
        print("Gathering tasks...")
        for t in self.app["tasks"]:
            kind = t["kind"]
            name = t["name"]
            idx = t["idx"]
            clock = self.clockMap[t["clockIdx"]]
            oPorts = []
            for p in t["oPorts"]:
                oPorts.append(OPort(self.variables[p["varIdx"]]))
            iPorts = []
            for p in t["iPorts"]:
                preds = []
                for pred in p["predecessors"]:
                    preds.append(SynchPred(pred["sourceNodeIdx"], self.variables[pred["variableIdx"]].getPort(), self.clockMap[pred["clockIdx"]]))
                iPorts.append(IPort(preds))
            funIdx = None
            if kind == "Functional":
                funIdx = t["funIdx"]
                self.tasks.append(self.funType(name, idx, funIdx, iPorts, clock, oPorts))
            else:
                assert False, "TODO: delay nodes. Need more genericity in the constructor definition"
        print("Done")

    def gatherFunctions(self):
        print("Gathering functions...")
        for f in self.app["functions"]:
            kind = f["kind"]
            idx = f["idx"]
            name = f["name"]
            if kind == "Monolithic":
                durationsMap = {}
                for d in f["durations"]:
                    durationsMap[d["procKind"]] = d["wcet"]
                durations = []
                for d in durationsMap:
                    durations.append(durationsMap[d])
                self.functions.append(MonolithicFunction(idx, name, durations))
            else:
                assert False, "TODO: support other kinds of function description"
        print("Done")

    def gatherArchitecture(self):
        print("Gathering Processors...")
        arch = self.app["architecture"]
        if "target" in arch:
            if arch["target"]=="STM32":
                self.archi = Architecture(Processor(0, 0))
                self.codeGen = STM32CodeGen(self.archi, self.tasks, self.functions, self.variables)
            elif arch["target"]=="TC275":
                assert False, "TODO: modelling TC275 + code generation"
                #self.archi = TC275()
                #self.codeGen = TC275CodeGen(self.archi, self.tasks, self.functions, self.variables)
        else:
            procs = []
            for p in arch["processors"]:
                procs.append(Processor(p["idx"], p["kind"]))
            self.archi = Architecture(procs)
            #TODO: check for DMA
        print("Done")

    def loadApp(self):
        self.gatherVarTypes()
        self.gatherVariables()
        self.gatherClocks()
        self.gatherTasks()
        self.gatherFunctions()
        self.gatherArchitecture()

    def defineScheduler(self):
        self.scheduler = self.schedType(self.tasks, self.archi, self.functions)

    def defineCodeGen(self, kind):
        if kind == "STM32":
            self.codeGen = STM32CodeGen(self.archi, self.tasks, self.functions, self.variables)

    def generate(self):
        self.scheduler.schedule()
        return self.codeGen.generateCoreCode(self.archi.procs[0])
    
sa = SynchronousApploader("testSync.json", "sa")
sa.loadApp()
sa.defineScheduler()
sa.defineCodeGen("STM32")
print(sa.generate())

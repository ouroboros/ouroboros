from z3 import *
from variables import *
#global variable
clockIdx = 0

######################################################################
#                                                                    #
#             Ouroboros module for synchronous clocks and variables  #
#             @author: T. Carle                                      #
#                                                                    #
######################################################################


# Dependency:
# - z3 package (linux package)
# - z3-solver python binding (via pip)

#####################################################################
# Variable description
#####################################################################

class BoolVar(ClockVar):
    """
    Boolean Variable
    """
    def __init__(self, idx, name, varType=None):
        ClockVar.__init__(self, idx, name, varType)
        self.formalVar = Bool(name)

    def __add__(self, other):
        """
        or
        """
        return OrExpression(self, other)

    def __mul__(self, other):
        """
        and
        """
        return AndExpression(self, other)

    def __invert__(self):
        """
        not
        """
        return NotExpression(self)

    def getClkDeps(self):
        return ClockDependency(self, Clock())
    
class IntVar(ClockVar):
    """
    Integer Variable
    """
    def __init__(self, idx, name, varType=None):
        ClockVar.__init__(self, idx, name, varType) 
        self.formalVar = Int(name)

    def __eq__(self, other):
        if type(other) == int:
            return EqVarInt(self, other)
        else:
            return EqVarVar(self, other)

    def __lt__(self, other):
        if type(other) == int:
            return InfVarInt(self, other)
        else:
            return InfVarVar(self, other)

    def __le__(self, other):
        if type(other) == int:
            return InfEqVarInt(self, other)
        else:
            return InfEqVarVar(self, other)

    def __ge__(self, other):
        if type(other) == int:
            return SupEqVarInt(self, other)
        else:
            assert False, "TODO"

    def __gt__(self, other):
        if type(other) == int:
            return SupVarInt(self, other)
        else:
            assert False, "TODO"
            
    def getClkDeps(self):
        return ClockDependency(self, Clock())
    
#####################################################################
# Clock system description
#####################################################################
class ClockDependency():
    """
    A type for clock dependencies: variables necessary to compute a
    clock, as well as the clocks when they are necessary
    """
    def __init__(self, var, clk):
        self.var = var
        self.clk = clk

    def __str__(self):
        return str(self.var)+ " @ " + str(self.clk)
        
    def getVar(self):
        return self.var

    def getClk(self):
        return self.clk

    def getClkDeps(self):
        return self.clk.getClkDeps()



class Clock():
    """
    A type for synchronous clocks
    """
    def __init__(self, setIdx = False, idx = 0):
        global clockIdx
        self.idx = idx
        if idx>clockIdx and setIdx:
            clockIdx = idx
            
        self.clockExpression = True
        self.baseClock = True
        self.clockDeps = []

    def __str__(self):
        return "clk"+ str(self.idx)

    def getIdx(self):
        return self.idx
    
    def getClkDeps(self):
        """
        Return a list of all clock dependencies
        """
        l = []
        for dep in self.clockDeps:
            l = [dep] + l
            l = dep.getClkDeps() + l
        return l
    
    def printClock(self):
        """
        For C code generation
        """
        if self.clockExpression == True:
            return "1"
        return "0"

    def __add__(self, other):
        """
        Union
        """
        if self.get() == True:
            return self
        if other.get() == True:
            return other
        return UnionClock(self, other)

    def __mul__(self, other):
        """
        Conjunction
        """
        if self.get() == True:
            return other
        if other.get() == True:
            return self
        return ConjunctionClock(self, other)

    def __truediv__(self, other):
        """
        Disjunction
        """
        return DisjunctionClock(self, other)

    def __lt__(self, expr):
        """
        Subsampling
        """
        return SubClock(self, expr)
        
    def get(self):
        return self.clockExpression
 
    def includes(self, other):
        """
        Does the clock include another clock, i.e. contains all its ticks ? i.e. other tick implies self tick
        """
        s= Solver()
        # We want to know if A => B, not just if there exists a
        # configuration satisfying A => B. So we check if there is no
        # configuration in which Not(A=>B) i.e. And(A, Not(B))
        s.add(And(other.get(), Not(self.get())))
        r = s.check()
        return r.r == -1

    def isIncluded(self, other):
        """
        Is the clock included in another clock ?
        """
        return other.includes(self)
   
    def isExcluded(self, other):
        """
        Are the clocks in exclusion ?
        """
        s = Solver()
        s.add(And(self.get(), other.get()))
        r = s.check()
        return r.r == -1

    def equivalent(self, other):
        """
        Are the clocks equivalent ?
        """
        s = Solver()
        s.add( Or( And( self.get(), Not(other.get())), And(Not(self.get()), other.get())))
        r = s.check()
        return r.r == -1
    
class UnionClock(Clock):
    """
    The union of two clocks
    """
    def __init__(self, c1, c2, setIdx = False):
        Clock.__init__(self, setIdx, clockIdx+1)
        self.clockExpression = Or(c1.get(), c2.get())
        self.c1 = c1
        self.c2 = c2
        self.baseClock = False
        self.clockDeps = c1.getClkDeps() + c2.getClkDeps()
        
    def printClock(self):
        """
        Printing for C code generation
        """
        if self.c1.get() == True or self.c2.get() == True:
            return "1"
        return "(" + self.c1.printClock() + " || " + self.c2.printClock() + ")"
    
class ConjunctionClock(Clock):
    """
    The conjunction of two clocks
    """
    def __init__(self, c1, c2, setIdx = False):
        Clock.__init__(self, setIdx, clockIdx+1)
        self.clockExpression = And(c1.get(), c2.get())
        self.c1 = c1
        self.c2 = c2
        self.baseClock = False
        self.clockDeps = c1.getClkDeps() + c2.getClkDeps()
        
    def printClock(self):
        """
        Printing for C code generation
        """
        if self.c1.get() == True:
            return self.c2.printClock()
        if self.c2.get() == True:
            return self.c1.printClock()
        return "(" + self.c1.printClock() + " && " + self.c2.printClock() + ")"
    
class DisjunctionClock(Clock):
    """
    C1\C2
    """
    def __init__(self, c1, c2, setIdx = False):
        Clock.__init__(self, setIdx, clockIdx+1)
        self.clockExpression = And(c1.get(), Not(c2.get()))
        self.c1 = c1
        self.c2 = c2
        self.baseClock = False
        self.clockDeps = c1.getClkDeps() + c2.getClkDeps()
        
    def printClock(self):
        """
        Printing for C code generation
        """
        return "(" + self.c1.printClock() + " && !" + self.c2.printClock() + ")"
    
class SubClock(Clock):
    """
    Downsampling of a clock (when)
    """
    def __init__(self, c1, b1, setIdx = False):
        Clock.__init__(self, setIdx, clockIdx+1)
        self.clockExpression = And(c1.get(), b1.get())
        self.c1 = c1
        self.b1 = b1
        self.baseClock = False
        self.clockDeps = c1.getClkDeps() + b1.getClkDeps()
        
    def printClock(self):
        """
        Printing for C code generation
        """
        if self.c1.get()==True:
            return self.b1.printClock()
        return "(" + self.c1.printClock() + " && " + self.b1.printClock() + ")"
    
class BoolExpression():
    """
    Boolean logic for clock construction
    """
    def __init__(self):
        self.expression=True
        self.clockDeps = []

    def getClkDeps(self):
        return self.clockDeps
    
    def printClock():
        """
        Printing for C code generation
        """
        return '1'
        
    def __add__(self, other):
        """
        or
        """
        return OrExpression(self, other)

    def __mul__(self, other):
        """
        and
        """
        return AndExpression(self, other)

    def __invert__(self):
        """
        not
        """
        return NotExpression(self)

    
    def get(self):
        return self.expression

    
class AndExpression(BoolExpression):
    """
    Boolean and
    """
    def __init__(self, b1, b2):
        BoolExpression.__init__(self)
        self.expression= And(b1.get(), b2.get())
        self.b1 = b1
        self.b2 = b2
        self.clockDeps = b1.getClkDeps() + b2.getClkDeps()
        
    def printClock(self):
        """
        Printing for C code generation
        """
        return "(" + self.b1.printClock() + " && " +self.b2.printClock() + ")"
    
class OrExpression(BoolExpression):
    """
    Boolean or
    """
    def __init__(self, b1, b2):
        BoolExpression.__init__(self)
        self.expression= Or(b1.get(), b2.get())
        self.b1 = b1
        self.b2 = b2
        self.clockDeps = b1.getClkDeps() + b2.getClkDeps()
        
    def printClock(self):
        """
        Printing for C code generation
        """
        return "(" + self.b1.printClock() + " || " +self.b2.printClock() + ")"
    
class NotExpression(BoolExpression):
    """
    Boolean not
    """
    def __init__(self, b1):
        BoolExpression.__init__(self)
        self.expression= Not(b1.get())
        self.b1 = b1
        self.clockDeps = b1.getClkDeps()
        
    def printClock(self):
        """
        Printing for C code generation
        """
        return "(!" + self.b1.printClock() + ")"
    
class IntExpression():
    """
    Integer expressions
    """
    def __init__(self):
        self.expression=None
        self.clockDeps = []
        
    def get(self):
        return None

    def getClkDeps(self):
        return self.clockDeps
    
class EqVarVar(IntExpression):
    """
    Equality between integer variables
    """
    def __init__(self, v1, v2):
        self.v1 = v1
        self.v2 = v2
        self.clockDeps = [v1.getClkDeps(), v2.getClkDeps()]#[ClockDependency(v1, Clock()), ClockDependency(v2, Clock())]

    def get(self):
        return self.v1.get() == self.v2.get()

    def printClock(self):
        """
        Printing for C code generation
        """
        return "("+ self.v1.printClock() + " == " + self.v2.printClock() + ")"
    
class EqVarInt(IntExpression):
    """
    Equality between an integer variable and an int constant
    """
    def __init__(self, v1, i1):
        self.v1 = v1
        self.i1 = i1
        self.clockDeps = [v1.getClkDeps()]#[ClockDependency(v1, Clock())]
        
    def get(self):
        return self.v1.get() == self.i1

    def printClock(self):
        """
        Printing for C code generation
        """
        return "(" + self.v1.printClock() + " == " + str(self.i1) + ")"
    
class InfVarVar(IntExpression):
    """
    Strict inferiority between integers
    """
    def __init__(self, v1, v2):
        self.v1 = v1
        self.v2 = v2
        self.clockDeps = [v1.getClkDeps(), v2.getClkDeps()]#[ClockDependency(v1, Clock()), ClockDependency(v2, Clock())]
        
    def get(self):
        return self.v1.get() < self.v2.get()

    def printClock(self):
        """
        Printing for C code generation
        """
        return "(" + self.v1.printClock() + " < " + self.v2.printClock() + ")"
    
class InfEqVarVar(IntExpression):
    """
    Inferiority between integers
    """
    def __init__(self, v1, v2):
        self.v1 = v1
        self.v2 = v2
        self.clockDeps = [v1.getClkDeps(), v2.getClkDeps()]#[ClockDependency(v1, Clock()), ClockDependency(v2, Clock())]
        
    def get(self):
        return self.v1.get() <= self.v2.get()

    def printClock(self):
        """
        Printing for C code generation
        """
        return "(" + self.v1.printClock() + " <= " + self.v2.printClock() + ")"
    
class InfVarInt(IntExpression):
    """
    Strict inferiority between integer var and int constant
    """
    def __init__(self, v1, i1):
        self.v1 = v1
        self.i1 = i1
        self.clockDeps = [v1.getClkDeps()]#[ClockDependency(v1, Clock())]
        
    def get(self):
        return self.v1.get() < self.i1

    def printClock(self):
        """
        Printing for C code generation
        """
        return "(" + self.v1.printClock() + " < " + str(self.i1) + ")"
    
class InfEqVarInt(IntExpression):
    """
    Inferiority between integer var and int constant
    """
    def __init__(self, v1, i1):
        self.v1 = v1
        self.i1 = i1
        self.clockDeps = [v1.getClkDeps()]#[ClockDependency(v1, Clock())]
        
    def get(self):
        return self.v1.get() <= self.i1

    def printClock(self):
        """
        Printing for C code generation
        """
        return "(" + self.v1.printClock() + " <= " + str(self.i1) + ")"

class SupEqVarVar(IntExpression):
    """
    Superiority between integer variables
    """
    def __init__(self, v1, v2):
        self.v1 = v1
        self.v2 = v2
        self.clockDeps = [v1.getClkDeps(), v2.getClkDeps()]#[ClockDependency(v1, Clock()), ClockDependency(v2, Clock())]
        
    def get(self):
        return self.v1.get() >= self.v2.get()

    def printClock(self):
        """
        Printing for C code generation
        """
        return "(" + self.v1.printClock() + " >= " + self.v2.printClock() + ")"
    

class SupEqVarInt(IntExpression):
    """
    Superiority between integer var and int constant
    """
    def __init__(self, v1, i1):
        self.v1 = v1
        self.i1 = i1
        self.clockDeps = [v1.getClkDeps()]#[ClockDependency(v1, Clock())]

    def get(self):
        return self.v1.get() >= self.i1

    def printClock(self):
        """
        Printing for C code generation
        """
        return "(" + self.v1.printClock() + " >= " + str(self.i1) + ")"

class SupVarVar(IntExpression):
    """
    Strict superiority between integer variables
    """
    def __init__(self, v1, v2):
        self.v1 = v1
        self.v2 = v2
        self.clockDeps = [v1.getClkDeps(), v2.getClkDeps()]#[ClockDependency(v1, Clock()), ClockDependency(v2, Clock())]

    def get(self):
        return self.v1.get() > self.v2.get()

    def printClock(self):
        """
        Printing for C code generation
        """
        return "(" + self.v1.printClock() + " > " + self.v2.printClock() + ")"
    
class SupVarInt(IntExpression):
    """
    Superiority between integer var and int constant
    """
    def __init__(self, v1, i1):
        self.v1 = v1
        self.i1 = i1
        self.clockDeps = [v1.getClkDeps()]#[ClockDependency(v1, Clock())]

    def get(self):
        return self.v1.get() > self.i1

    def printClock(self):
        """
        Printing for C code generation
        """
        return "(" + self.v1.printClock() + " > " + str(self.i1) + ")"

# iv0 = IntVar(2, 'i0')
# iv1 = IntVar(3, 'i1')


# ieq0 = iv0 <= 1#InfEqVarInt(iv0, 1)
# eq1 = iv0 == iv1#EqVarVar(iv0, iv1)


# bv0 = BoolVar(0, 'b0')
# bv1 = BoolVar(1, 'b1')


# e = bv0 + bv1
# c0 = Clock()


# c2 = c0 < bv0
# c3 = c0 < bv1#SubClock(c0, bv1)
# c4 = c3 / c2

# solver = Solver()
# solver.add(c4.get())


# res = solver.check()
# print("Resultat: " + str(res.r))
# print(solver)

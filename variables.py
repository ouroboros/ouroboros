######################################################################
#                                                                    #
#             Ouroboros module for variable description              #
#             @author: T. Carle                                      #
#                                                                    #
######################################################################

# General description of variables for code generation. See sClocks.py
# for the use of variables in sybchronous clocks.


#####################################################################
# Variable description
#####################################################################

class VariableType():
    """
    A variable type is composed of the index and name of the type and (optional) its size in octets
    """
    def __init__(self, idx, name, size=None):
        self.idx = idx
        self.name = name
        self.size = size

    def getName(self):
        return self.name

    def getSize(self):
        return self.size

class Variable():
    """
    A variable is composed of an index, a name and a type
    """
    def __init__(self, idx, name, varType=None):
        self.idx = idx
        self.name = name
        self.varType = varType
        self.port = None
        
    def getIdx(self):
        return self.idx

    def getVarType(self):
        return self.varType

    def getVarTypeName(self):
        return self.varType.name
        
    def getName(self):
        return self.name

    def getPort(self):
        return self.port

    def setPort(self, port):
        self.port = port

    def __str__(self):
        return self.name
class ClockVar(Variable):
    """
    Virtual variable class for variables which are used to express clocks
    """
    def __init__(self, idx, name, varType=None):
        Variable.__init__(self, idx, name, varType)
        self.formalVar = None

    def printClock(self):
        """
        Printing variable name for C code generation
        """
        return self.name
    
    def get(self):
        return self.formalVar

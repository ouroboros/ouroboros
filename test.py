from scheduler import *
from archInterference import *



# t0= DataflowNode("t0",0,0,[])#, [AbstractAddress(0)])
# t1= DataflowNode("t1",1,1,[])#, [AbstractAddress(0)])
# t2= DataflowNode("t2",2,2,[Predecessor(0)])#,[AbstractAddress(0)])
# t3= DataflowNode("t3",3,3,[Predecessor(1), Predecessor(2)])#,[AbstractAddress(0)])
# t4= DataflowNode("t4",4,2,[Predecessor(1)])#,[AbstractAddress(0)])
# t5= DataflowNode("t5",5,1,[])#,[AbstractAddress(0)])
# t6= DataflowNode("t6",6,4,[Predecessor(0), Predecessor(3), Predecessor(4), Predecessor(5)])#,[AbstractAddress(0)])

# tasks= [t0, t1, t2, t3, t4, t5, t6]

t0=PeriodicTask("t0",0,0,5)
t1=PeriodicTask("t1",1,1,10)
t2=PeriodicTask("t2",2,2,20)

tasks=[t0,t1,t2]

f0= MonolithicFunction(0, "f0", [1, 2], [[(AbstractAddress(0), 1)], [(AbstractAddress(0), 2)]])
f1= MonolithicFunction(1, "f1", [2, 1], [[(AbstractAddress(0), 1)], [(AbstractAddress(0), 2)]])
f2= MonolithicFunction(2, "f2", [1, 1], [[(AbstractAddress(0), 1)], [(AbstractAddress(0), 2)]])
# f3= MonolithicFunction(3, "f3", [1, 4], [[(AbstractAddress(0), 1)], [(AbstractAddress(0), 1)]])
# f4= MonolithicFunction(4, "f4", [1, 2], [[(AbstractAddress(0), 1)], [(AbstractAddress(0), 1)]])

functions = [f0, f1, f2]

# f0= SegmentFunction(0, "f0", [[4, 1, 3], [2, 2, 2, 2]], [[[(AbstractAddress(0),2)], [], [(AbstractAddress(0),1)]],[[(AbstractAddress(0), 2)], [(AbstractAddress(0),1)], [(AbstractAddress(0), 2)], [(AbstractAddress(0),1)]]])
# f1= SegmentFunction(1, "f1", [[4, 1, 3], [2, 2, 2, 2]], [[[(AbstractAddress(0),2)], [], [(AbstractAddress(0),1)]],[[(AbstractAddress(0), 2)], [(AbstractAddress(0),1)], [(AbstractAddress(0), 2)], [(AbstractAddress(0),1)]]])
# f2= SegmentFunction(2, "f2", [[4, 1, 3], [2, 2, 2, 2]], [[[(AbstractAddress(0),2)], [], [(AbstractAddress(0),1)]],[[(AbstractAddress(0), 2)], [(AbstractAddress(0),1)], [(AbstractAddress(0), 2)], [(AbstractAddress(0),1)]]])
# f3= SegmentFunction(3, "f3", [[4, 1, 3], [2, 2, 2, 2]], [[[(AbstractAddress(0),2)], [], [(AbstractAddress(0),1)]],[[(AbstractAddress(0), 2)], [(AbstractAddress(0),1)], [(AbstractAddress(0), 2)], [(AbstractAddress(0),1)]]])
# f4= SegmentFunction(4, "f4", [[4, 1, 3], [2, 2, 2, 2]], [[[(AbstractAddress(0),2)], [], [(AbstractAddress(0),1)]],[[(AbstractAddress(0), 2)], [(AbstractAddress(0),1)], [(AbstractAddress(0), 2)], [(AbstractAddress(0),1)]]])


# functions = [f0, f1, f2, f3, f4]

p0 = Processor(0, 0)
#p1 = Processor(1, 1)

#procs= [p0, p1]
procs = [p0]
pIdxList = [0]
#pIdxList = [0, 1]
arch= Architecture(procs)

#busPenalty = 1
#bus = Interconnect([Arbiter(0, busPenalty, pIdxList)])
#archi = GenericMulticoreWithBus(arch, bus)


# archi=Architecture(procs)
#sched=DataflowMonolithic(tasks, archi, functions)

#sched= DataflowMonolithic(tasks, arch, functions)

sched=RMScheduler(tasks,arch, functions)

sched.schedule()
print(sched)
nArchi = archi.applyInterferences(0)

sched.setArchi(nArchi)
print(sched)

# from synchronousApps import *

# c0 = Clock()

# v0 = Variable(0)
# v1 = Variable(1)

# f0 = Function("f", 0)
# f1 = Function("g", 1)
# f2 = Function("h", 2)


# oPort0 = OPort(v0)
# oPort1 = OPort(v1)

# n0 = SynchronousFunctional("node0", 0, [], c0, [oPort0], f0)
# n1 = SynchronousFunctional("node1", 1, [], c0, [oPort1], f1)

# pred0 = SynchPred(0, oPort0, c0)
# pred1 = SynchPred(1, oPort1, c0)

# iPort0 = IPort([pred0])
# iPort1 = IPort([pred1])

# n2 = SynchronousFunctional("node2", 2, [iPort0, iPort1], c0, [], f2)

# p0 = Processor(0, [2, 1, 10])
# p1 = Processor(1, [2, 1, 10])

# procs = [p0,p1]

# archi = Architecture(procs)

# sched = SynchronousDFScheduler([n0, n1, n2], archi)

# sched.schedule()
# print(sched)

from tasks import *
from scheduler import *
from sClocks import *

######################################################################
#                                                                    #
#             Ouroboros module for synchronous semantics             #
#             @author: T. Carle                                      #
#                                                                    #
######################################################################


#####################################################################
# Dataflow description
#####################################################################

class OPort():#TODO : is it necessary, or can we directly use variables ?
    def __init__(self, variable):
        #Should we work with indices instead ?
        self.variable = variable
        self.variable.setPort(self) # Each variable is produced by
        #exactly one port (SSA-style), and contains a reference to
        #this port. We set this reference here.
        self.taskIdx = None
        
    def getVariable(self):
        return self.variable

    def setTaskIdx(self, taskIdx):
        self.taskIdx = taskIdx

    def getTaskIdx(self):
        return self.taskIdx
    
class IPort():
    """
    An input port for a synchronous dataflow node
    """
    def __init__(self, preds):
        self.inputs= preds

    def __str__(self):
        st= "IPort: "
        for i in self.inputs:
            st += "\n\t" + str(i)
        return st
    
    def getPreds(self):
        l=[]
        for p in self.inputs:
            l.append(p)
        return l
 

#####################################################################
# Synchronous semantics wizard
#####################################################################
class SynchPred(Predecessor):
    """Represents a dependence arc in a synchronous graph (from one
    output port/variable to one input port, with a
    condition). Builds upon a predecessor class
    
    """
    def __init__(self, idx, oPort, clock):
        Predecessor.__init__(self, idx)
        self.oPort = oPort
        self.clock = clock
        
        #TODO : add methods to deal with delays
        
    def getOPort(self):
        return self.oPort

    def getVar(self):
        return self.oPort.getVariable()
    
    def getVarName(self):
        return self.oPort.getVariable().getName()

    def getClock(self):
        return self.clock

    
class SynchronousSemantics():
    def __init__(self):
        self.nom= "Synchronous Semantics Deus ex Machina"

    def SynchronousNodeTypes(self, t):
        """
        A creator of dynamic inheritage classes to apply synchronous semantics to nodes of type t
        """
        class SynchronousNode(t):
            """
            Virtual class
            """
            def __init__(self, name, idx, preds, clock, outputPorts, funIdx=None):
                t.__init__(self, name, idx, funIdx, preds)
                self.clock = clock
                self.oPorts = outputPorts
                for op in outputPorts:
                    op.setTaskIdx(idx)
                self.functional = True
                
            def getPreds(self): # Overloading
                l=[]
                for i in self.preds:
                    #print( str(i) )
                    
                    # Add predecessors
                    l2 = i.getPreds()
                    # Add predecessors for input clocks computations (impose endochrony)
                    l3 = []
                    for p in l2:
                        deps = p.getClock().getClkDeps()
                        for d in deps:
                            l3.append(SynchPred(d.getVar().getPort().getTaskIdx(), d.getVar().getPort(), d.getClk()))
                    
                    l+= l2 + l3
                #Add predecessors for clock computation
                for d in self.clock.getClkDeps():
                    l.append(SynchPred(d.getVar().getPort().getTaskIdx(), d.getVar().getPort(), d.getClk()))
            
                return l

            def getIPorts(self):
                return self.preds # encapsulating the refined model of input ports
    
            def getClock(self):
                return self.clock

            def getOPorts(self):
                return self.oPorts

            def getFunctional(self):
                return self.functional
            
        class SynchronousDelay(SynchronousNode):
            """Basic synchronous delay. When building the delay we can specify no
            input port, and set it later, once all nodes have been declared
            
            """
            def __init__(self, name, idx, clock, outputPort, initVar, inputPort=[]):
        
                SynchronousNode.__init__(self, name, idx, inputPort, clock, outputPort)
                self.initialVal = initVar
                self.functional = False
                
            def setInputPort(self, inputPort):
                self.preds = [inputPort] # For now we keep the input port as a list with just one element (maybe we will take out the list later on)
        
        class SynchronousFunctional(SynchronousNode):
            """
            Functional node for synchronous dataflow applications
            """
            def __init__(self, name, idx, funIdx , syncPreds, clock, outputPorts):
                SynchronousNode.__init__(self, name, idx, syncPreds, clock, outputPorts, funIdx)
                self.function = funIdx
                
            def getFunction(self):
                return self.function
            
        return SynchronousNode, SynchronousDelay, SynchronousFunctional

    def synchronousScheduler(self, t):
        """
        A creator of dynamic heritage class for scheduling under synchronous semantics
        """
        class SynchronousDFScheduler(t):
            """
            A dataflow scheduler for applications with synchronous semantics
            """
            def __init__(self, tasks, archi, functions):
                self.allNodes = tasks # All nodes, including delays, so we can add data transfers, generate code, etc.
                
                # Delays are not going to be scheduled
                functionalTasks = []
                delayNodes = []
                for ta in tasks:
                    if ta.getFunctional() == True:
                        functionalTasks.append(ta)
                    else:
                        delayNodes.append(ta)
                        # Building the initial state of the scheduler
                t.__init__(self, functionalTasks, archi, functions) # Only the functional nodes will be scheduled
                self.delayNodes = delayNodes
            # def predsReady(self, taskIdx):
            #     """
            #     Overloading to include synchronous semantics: delay blocs have a particular semantics
            #     """
            #     #TODO: Deal with clocked dependencies ?
            #     for p in self.tasks[taskIdx].getPreds():
            #         if type(self.allNodes[p.getIdx()]) != SynchronousDelay:
            #             return False
            #         return True
            
        return SynchronousDFScheduler



    
    
# def test(t):
#     class Test(t):
#         def __init__(self, name, idx, funIdx, preds):
#             t.__init__( self,name, idx, funIdx, preds)

#     return Test
        
# n = DataflowNode("toto", 1, 1, [])

# Test = test(DataflowNode)
# n2 = Test( "tata", 2, 2, [])

# print(str(n2))

:orphan:

static_scheduler
================

.. toctree::
   :maxdepth: 4

   architectures
   data_structures
   scheduler
   tasks

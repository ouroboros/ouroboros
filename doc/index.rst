.. Ouroboros documentation master file, created by
   sphinx-quickstart on Tue Mar 10 09:58:48 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Ouroboros's documentation!
=====================================

Modules
=======

.. toctree::
   :maxdepth: 2

   modules/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


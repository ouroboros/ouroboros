from scheduler import *
from dma import *

####################################################################################################
#                                                                                                  #
#                  Ouroboros module for basic AER (Processor-based PREM) scheduling                #
#                                                                                                  #
#                                   @author: T. Carle                                              #
#                                                                                                  #
####################################################################################################

#----------------------------------------#
# Classes for description of AER setting #
#                                        #
# We consider a multicore with local     #
# memories to each core which can be     #
# loaded by the cores. The shared        #
# memory is divided into pages of equal  #
# size which can be loaded in the local  #
# memories, with a direct mapping        #
# (associativity of 1) model             #
#----------------------------------------#

#TODO: allow interference computation when exclusion is not enforced
#between A and R phases -- requires including a memory profile on the
#scheduled A and R phases, but interference computation expects one
#slot per job -- except with segment based tasks !!! => See AER as a 3
#segment task ?

#TODO: Suppress R phases when all successors are on the same processor

##############################################
# Tasks
##############################################

class AERDFNode(DataflowNode):
    """
    A task with paging information which generates jobs tailored for AER scheduling
    """
    def __init__(self, name, idx, funIdx, preds):
        DataflowNode.__init__(self, name, idx, funIdx, preds)

    def generateJobs(self, datesAndProcs):
        self.jobs.append(AERJob(0, self.idx, datesAndProcs))
        
class AERJob(Job):
    """
    Job description for AER tasks
    """
    def __init__(self, idx, taskIdx, datesAndProcs):
        Job.__init__(self, idx, taskIdx, 1, datesAndProcs)
        self.pages = None
        self.ASlot = None
        self.RSlot = None


    def addRSlot(self, rSlot):
        self.RSlot = rSlot

    def addASlot(self, aSlot):
        self.ASlot = aSlot

    def getEndDate(self): # End of a job is infinite if not scheduled, else is end date of Restitution time slot
        if self.timeSlots==[]:
            return ExtendedInt(0, True)
        else:
            if self.RSlot != None:
                return self.RSlot.getEnd()
            else:
                return self.timeSlots[-1].getEnd()
        
    def setPages(self, pages):
        """
        To be set a posteriori once the job is scheduled
        """
        self.pages = pages

        
        
##################################################
#  Schedule entities
##################################################
    
class AERJobSchedulingResult(JobSchedulingResult):
    def __init__(self, res, procIdx, dmaIdx): # res are ordered: ([OrderAcq, TransAcq, Exe, OrderRest, TransRest],pages)
        intervals, pages = res
        Eints = []
        for i in intervals[2:-2]:
            Eints.append(i.getInterval())
        JobSchedulingResult.__init__(self, Eints, procIdx)
        self.dmaIdx = dmaIdx # None for AER, integer for REW
        self.pages = pages
        self.dmaIntervals = [intervals[1]]+[intervals[-1]]
        self.dmaOrderIntervals = [intervals[0]]+[intervals[-2]]

    def __str__(self):
        return "Scheduling result: dmaOrderIntervals: "+ self.dmaOrderIntervals[0] + "; " + self.dmaOrderIntervals[1]
        
    def getDmaIdx(self):
        return self.dmaIdx

    def getDmaIntervals(self):
        return self.dmaIntervals

    def getDmaOrderIntervals(self):
        return self.dmaOrderIntervals
    
    def getEnd(self):
        if self.dmaIntervals[1] != None:
            return self.dmaIntervals[1].getInterval().getEnd()
        else: # if there is no transaction for restitution, then there is no order for restitution either
            return self.intervals[-1].getEnd()

    def getPages(self):
        return self.pages
    

####################################################
# Scheduler
####################################################


class AER_DF_Monolithic(DataflowMonolithic):

    def __init__(self, tasks, archi, funs):
        DataflowMonolithic.__init__(self, tasks, archi, funs)
        self.sequencer = Processor(0, 0) # Used to impose sequencing of A and R phases (for now we consider they must all be sequenced, regardless of the interconnect topology)
        
    def sequencerValidate(self, interval):
        """
        Encapsulation of Processor free interval query
        """
        return self.sequencer.getNextFreeIntervalWhole(interval.getInterval().getStart(), interval.getInterval().getSize())
    
    def sequencerRemove(self, interval):
        """
        Encapsulation of Processor free interval removal
        """
        self.sequencer.removeFreeInterval(interval.getInterval().getStart(), interval.getInterval().getSize())
        
    def schedASAPMulticore(self, proc, job, budget, preemptive, interconnect,  sDate = 0):
        """
        Schedules the job as 3 parts A, E and R as soon as
        possible. budget corresponds to the Exec portion WCET.

        """
        dateMin = job.getReadyDateMulticore()
        funIdx = self.tasks[job.getTaskIdx()].getFunIdx()
        pages = self.functions[funIdx].getMemPages(proc.getProcKind())
        exclusion = True # For now we always do aer in exclusion
        return self.schedASAPMulticoreNoDMA(proc, budget.getBudget()[0], preemptive, dateMin, exclusion, funIdx, pages)


    def schedASAPMulticoreNoDMA(self, proc, budget, preemptive, dateMin, exclusion, funIdx, jobPages):
        """
        Schedule using AER model without dma. Exclusion parameter specifies if the A and R phases must be scheduled in exclusion or not.
        """
        
        function = self.functions[funIdx]
        sizeA, sizeR = function.getTransferSizes(proc.getProcKind())
        (budgetA, budgetR) = proc.getTransferBudget(sizeA), proc.getTransferBudget(sizeR)

        if budgetA.isInfinite():
            #If Acquisition budget is infinite, it is impossible to schedule to this core
            return [None, None, None, None, Interval(0, ExtendedInt(0,True))]
        #Acquisition is just the transfer
        intervalA = None
        intervalE = None
        #Restitution is just the transfer
        intervalR = None
        found = False
        while not found:
            found = True
            #Step 1: find a slot for Acquisition on the processor
            #if not budgetA.isInfinite():
            if not budgetA==0:
                if not exclusion:
                    intervalA = proc.getNextFreeIntervalWhole(dateMin, budgetA, jobPages)#Acquisition is not preemptive
                    
                else:
                    # In exclusion, we must use the scheduler
                    # sequencer to find intervals where no acquisition
                    # or restitution are performed
                    found = False
                    while not found:
                        
                        intervalA = proc.getNextFreeIntervalWhole(dateMin, budgetA, jobPages)#Acquisition is not preemptive
                        iSeq = self.sequencerValidate(intervalA)
                        if iSeq.getStart() == intervalA.getInterval().getStart():
                            found = True
                        else:
                            dateMin = iSeq.getStart()
                            
                dateMin = intervalA.getInterval().getEnd()
                jobPages= intervalA.getPages()
            #Step 2: find a slot for execution on the procesor
            if preemptive:
                assert False, "TODO"
            else:
                intervalE = proc.getNextFreeIntervalWhole(dateMin, budget, jobPages)
                jobPages= intervalE.getPages()
            #Step 3: find a slot for Restitution on DMA
            #if not budgetR.isInfinite():
            if not budgetR==0:
                if not exclusion:
                    
                    intervalR = proc.getNextFreeIntervalWhole(intervalE.getInterval().getEnd(), budgetR, jobPages)#Restitution is not preemptive
                else:
                    # In exclusion, we must use the scheduler
                    # sequencer to find intervals where no acquisition
                    # or restitution are performed
                    found = False
                    dateMin = intervalE.getInterval().getEnd()
                    while not found:
                        intervalR = proc.getNextFreeIntervalWhole(dateMin, budgetR, jobPages)#Acquisition is not preemptive
                        iSeq = self.sequencerValidate(intervalR)
                        if iSeq.getStart() == intervalR.getInterval().getStart():
                            found = True
                        else:
                            dateMin = iSeq.getStart()
            #Step 4: verify that memory pages can be reserved without interruption from the start of A till the end of R
            testStart = 0 
            testEnd = 0
            if intervalA != None:
                testStart = intervalA.getInterval().getStart()
            else:
                testStart = intervalE.getInterval().getStart()

            if intervalR != None:
                testEnd = intervalR.getInterval().getEnd()
            else:
                testEnd = intervalE.getInterval().getEnd()

            testInterval = proc.getNextFreeIntervalWhole(testStart, testEnd - testStart, jobPages)
            if testInterval.getInterval().getStart() != testStart:
                found = False
                dateMin = testInterval.getInterval().getStart()
        return ([None, intervalA, intervalE, None, intervalR], intervalE.getPages())#interval for orders do not exist in this setting

    def scheduleJobNonPreemptiveMulticore(self, readyJob):
        res=None
        # Determine which mapping minimises the end date of the job
        funIdx = self.tasks[readyJob.getTaskIdx()].getFunIdx()
        for p in self.archi.getProcs():
            budget=self.getBudget(p.getIdx(), readyJob.getTaskIdx())
            if(not budget.isInfinite()):
                candIntervals = self.schedASAPNonPreemptiveMulticore(p, readyJob, budget, self.archi.getInterconnect())
                res2 = AERJobSchedulingResult(candIntervals, p.getIdx(), None)
                if not res2.getEnd().isInfinite() and(res==None or (res.getEnd()>res2.getEnd())):
                    res = res2
        return res

    def applySchedulingDecision(self, readyJob, result):
        #Step 0: on job
        readyJob.setPages(result.getPages())
        
        #Step 1: on processor and sequencer schedule
        trInt = result.getDmaIntervals()#We use a data structure for DMA, but it works the same for transfers without DMA
        
        #Step 1a: acq, if any
        start = None
        if trInt[0] != None:
            start = trInt[0].getInterval().getStart()
            tSlotAcq = DMACompTime(start, trInt[0].getInterval().getSize(), readyJob, readyJob.getTaskIdx(), result.getProcIdx(), None, True)
            self.getProcs()[result.getProcIdx()].updateSchedule(tSlotAcq)
            # Update scheduler sequencer with acq interval
            self.sequencerRemove(trInt[0])
        else:
            start = result.getIntervals()[0].getStart()
            
        #step 1b: exe
        Scheduler.applySchedulingDecision(self, readyJob, result)

        #step 1c: res, if any
        end = None
        if trInt[1] != None:
            end = trInt[1].getInterval().getEnd()
            tSlotAcq = DMACompTime(trInt[1].getInterval().getStart(), trInt[1].getInterval().getSize(), readyJob, readyJob.getTaskIdx(), result.getProcIdx(), None, False)
            self.getProcs()[result.getProcIdx()].updateSchedule(tSlotAcq)
            # Update scheduler sequencer with res interval
            self.sequencerRemove(trInt[1])
        else:
            end = result.getIntervals()[-1].getEnd()
        
        #Step 2: on processor memory allocation
        self.archi.getProcs()[result.getProcIdx()].updateLocalMem(start, end-start, result.getPages())

        #Step3: TODO: adapt following to get memory description of acquisition in task description (in case of non exclusion, allows computation of interference)
            #funIdx = self.tasks[readyJob.getTaskIdx()].getFunIdx()
            #if(self.functions[funIdx].isDefMemProfile()):
            #    memProfile = self.functions[funIdx].getMemProfile(self.archi.getProcs()[result.getProcIdx()].getType())
            #    readyJob.updateMemAccess(memProfile)


#####################################################
# Test
#####################################################

# !!!! Model where pages are specified: works with AER_DF_Monolithic scheduler
# f0 = DMAMonolithicFunction(0, "f0", [2, 5], [[], []], [0, 2], [0, 2], [[0, 1],[2]]) # Idx, name, durations on processor kinds, memory access profiles, acq sizes, rest sizes, memory "pages"
# f1 = DMAMonolithicFunction(1, "f1", [1, 1], [[], []], [1, 1], [1, 1], [[2],[0]])
# f2 = DMAMonolithicFunction(2, "f2", [5, 3], [[], []], [5, 3], [2, 2], [[0],[1]])
# f3 = DMAMonolithicFunction(3, "f3", [2, 2], [[], []], [2, 0], [1, 0], [[1],[2]])

# !!!! Model where pages are unspecified, but number of adjacent pages is specified: works with REW_unfixed_DF_Monolithic scheduler
f0 = DMAMonolithicFunction(0, "f0", [2, 5], [[], []], [0, 2], [0, 2], [(2,0), (1,0)]) # Idx, name, durations on processor kinds, memory access profiles, acq sizes, rest sizes, number of memory "pages"
f1 = DMAMonolithicFunction(1, "f1", [1, 1], [[], []], [1, 1], [1, 1], [(1,0), (1,0)])
f2 = DMAMonolithicFunction(2, "f2", [5, 3], [[], []], [5, 3], [2, 2], [(1,0), (1,0)])
f3 = DMAMonolithicFunction(3, "f3", [2, 2], [[], []], [2, 0], [1, 0], [(1,0), (1,0)])


functions = [f0, f1, f2, f3]

n0 = AERDFNode("n0", 0, 0, [])
n1 = AERDFNode("n1", 1, 1, [])
n2 = AERDFNode("n2", 2, 2, [Predecessor(0)])
n3 = AERDFNode("n3", 3, 3, [])
tasks = [n0, n1, n2, n3]

p0 = DMAProcI2D(0, 3, 0, 1)
p1 = DMAProcI2D(1, 3, 1, 1)
processors = [p0, p1]

archi = DMAArchi(processors, [])

sched = AER_DF_Monolithic(tasks, archi, functions)
sched.schedule()
#print(sched)

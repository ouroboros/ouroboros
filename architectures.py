from data_structures import *

######################################################################
#                                                                    #
#             Ouroboros module for architecture description          #
#             @author: T. Carle                                      #
#                                                                    #
######################################################################

class Processor():
    def __init__(self, idx, processorKind):
        self.idx=idx
        self.schedule= Schedule()
        #self.budgets=[]
        self.processorKind = processorKind
        # for b in budgets: # budget specified as 0 is infinite i.e task is not schedulable on this processor
        #     if b==0:
        #         self.budgets.append(ExtendedInt(0, True))
        #     else:
        #         self.budgets.append(ExtendedInt(b, False))

    def __str__(self):
        res="Processor "+ str(self.idx)+":\n"
        node=self.schedule.getTimeSlots()
        while(node!=None):
            res+="\t"+ str(node) + "\n"
            node=node.getNext()
        return res

    def getIdx(self):
            return self.idx
            
    def getSchedule(self):
        return self.schedule.getTimeSlots()

    #def getBudgets(self, idx):
    #    return self.budgets[idx]

    def getProcKind(self):
        return self.processorKind

    def getType(self):# Alias for getProcKind
        return self.processorKind
    
    def getNextFreeInterval(self, dateMin, budget, pages = None):
        return self.schedule.getNextFreeInterval(dateMin, budget)

    def getNextFreeIntervalWhole(self, dateMin, budget, pages = None):
        return self.schedule.getNextFreeIntervalWhole(dateMin, budget)

    def removeFreeInterval(self, date, size, pages = None):
        self.schedule.removeFreeInterval(date, size)

    def updateSchedule(self, timeSlot):
        self.schedule.updateSchedule(timeSlot)

    def cloneUpTo(self, date):
        nProc = Processor(self.idx, self.processorKind)
        newSchedule,nextSlot = self.schedule.cloneUpTo(date)
        nProc.schedule = newSchedule
        return nProc,nextSlot

    def getRemainderSlots(self, date):
        temp = self.schedule.getTimeSlots()
        if temp.getVal() == None: # first Ll node is sentinel
            temp=temp.getNext()
        while temp != None and temp.getStart()<date:
            temp=temp.getNext()
        return temp

    def getSlot(self, date):
        """
        Returns the slot active at date on the Processor, or None if there is no slot active at the date
        """
        return self.schedule.getSlotAt(date)

    def getSlotBefore(self, date):
        """
        Returns the last slot that finishes before date
        """
        return self.schedule.getSlotBefore(date)

    def getSlotAfter(self, date):
        """
        Returns the first slot that starts after date
        """
        return self.schedule.getSlotAfter(date)
    
    def applyInt(self, slotCopy, penalty):
        """
        Increase the time budget of a slot by the value of the penalty. TODO: Should we use a separate class just to include this method ?
        """
        sl = self.getSlot(slotCopy.getStart())
        # remove the free interval corresponding to the penalty
        self.schedule.removeFreeInterval(sl.getEnd(), penalty)
        # increase the interference budget in the reserved time slot
        sl.increaseIFBudget(penalty)
        
        
    def getEndDate(self):
        return self.schedule.getEndDate()



    
class Architecture():
    """A generic architecture model with only processors/cores and
    perfect interconnect (i.e. no interference). All methods for
    interference computation are designed with a linear recomputation
    in mind (rescheduling slots so that each slot we re-schedule is
    temporarily the last slot in the schedule). They can be overloaded
    if needed for more complex interference analysis/scheduling
    methods.

    """
    def __init__(self, procs, scheduler=None):
        self.procs=procs
        self.scheduler = scheduler # A hook to the scheduler so that we can use the chosen scheduling policy when recomputing the schedules with interference. I need to put it here for genericity in the scheduler basic methods

    def __str__(self):
        res=""
        for p in self.getProcs():
            res+=str(p)+"\n\n"
        return res
    
    def getProcs(self):
        return self.procs

    def setScheduler(self, scheduler): # here for genericity of the scheduler methods
        self.scheduler=scheduler

    def buildNew(self, nProcs, scheduler):
        """
        Virtual: must be overloded in each legacy class
        """
        return Architecture(nProcs, scheduler)
    
class Interconnect():
    """
    A general class for defining interconnects in the broad sense of the term.
    """
    def __init__(self, resources):
        """Resources here are used to model resources in the interconnect
        that can be represented as sequentially or parallely reserved in time (such
        as buses for message passing, router multiplexers in NoCs, or memory bank arbiters)

        Resources MUST be a list no matter the number of resources
        """
        self.resources = resources

    def getResources(self):
        return self.resources

    def concernedResources(self, abstractAddress):
        """This function returns a list of the resources of the interconnect
        that correspond to the abstract address. An abstract address
        can be a real memory address, a memory bank index,
        etc. depending on the level of abstraction used (for
        compiling, real addresses are needed, but for experiments one
        can model accesses to memory banks only).

        For now, we consider only interconnects composed of one or
        multiple arbiters directly linked to memory banks: no NoC routing.

        """
        result=[]
        for res in self.resources:
            if res.isConcerned(abstractAddress):
                result.append(res)
        return result

    def clone(self, remain):
        nres = []
        for res in self.resources:
            nres.append(res.clone(remain))
        return Interconnect(nres)



from scheduler import *
from functions import *
######################################################################
#                                                                    #
#             Ouroboros module for DMA and paged memory              #
#             @author: T. Carle                                      #
#                                                                    #
######################################################################

####################################
# Contains mainly architectural    #
# models and support for scheduling#
# but no scheduler, because the    #
# presence of DMA does not         #
# define by itself a complete      #
# execution model                  #
####################################

####################################
# Also models caches with a        #
# special execution model:         #
# data is pre-loaded before        #
# execution, and invalidated at    #
# the end of the job execution     #
####################################

####################################
# Architectural elements
####################################

class DMA(Processor):
    """DMAs are modelled as processors with particular properties:  they have two schedule lines: one for the transfers, and one for receiving transfer orders from the cores

    """
    def __init__(self, idx, transferRate):
        Processor.__init__(self, idx, None) #No need to specify a processor kind
        self.targetCore = None
        self.coreOrders = Schedule()
        self.transferRate = transferRate
    
    def __str__(self):
        res = "DMA "+str(self.idx)+":\n"
        node = self.schedule.getTimeSlots()
        while(node != None):
            res+="\t"+str(node)+ "\n"
            node=node.getNext()
        return res

    def getTransferBudget(self, size):
        return size*self.transferRate

    
    def setTargetCore(self, core):
        """
        Temporarily set the core (pointer, not idx) that initiates the transaction. Set to None when done.
        """
        self.targetCore = core

    def getBudgetOrder(self, funIdx):
        """
        Get the budget of writting to the DMA for a given task
        """
        return self.targetCore.getOrderBudgets(funIdx)
        
    def getNextFreeIntervalOrders(self, dateMin, budget):
        """
        Getting the next free interval on the processor orders line: we forbid preemptions on this line
        """
        return self.getNextFreeIntervalWholeOrders(dateMin, budget)

    def getNextFreeIntervalWholeOrders(self, dateMin, budget):
        """
        Getting the next free interval on the processor orders line
        """
        return self.coreOrders.getNextFreeIntervalWhole(dateMin, budget)

    def updateScheduleOrders(self, timeSlot):
        """
        Update the reservations for the schedule of writes to the DMA
        """
        self.coreOrders.updateSchedule(timeSlot)
        
    def getNextFreeInterval(self, dateMin, budget, pages):
        """
        Getting a free interval on the transfer line
        For now, we do not allow multiple separate transfers for the same task
        """
        return self.getNextFreeIntervalWhole(dateMin, budget, pages)
    
    def getNextFreeIntervalWhole(self, dateMin, budget, pages):
        """
        Must be a mix between availability on the DMA transfer line and in the memory
        """
        assert self.targetCore != None, "DMA was not initialized with a core"
        
        currentInt = None
        found = False
        while not found:
            found = True
            tempInt = Processor.getNextFreeIntervalWhole(self, dateMin, budget)
            currentInt = self.targetCore.getNextFreeMemoryIntervalWhole(tempInt.getStart(), tempInt.getSize(), pages)
            if currentInt.getInterval().getStart()!=tempInt.getStart():
                found = False
                dateMin = currentInt.getInterval().getStart()

        return currentInt

    def getNextFreeIntervalWholeVirtual(self, dateMin, budget, pages, Tints):
        """Must be a mix between availability on the DMA transfer line and in
        the memory. Must be careful with Tints which are candidates
        for reservation, but not yet updated in the free intervals bank.

        """
        assert self.targetCore != None, "DMA was not initialized with a core"
        
        currentInt = None
        found = False
        while not found:
            found = True
            tempInt = Processor.getNextFreeIntervalWhole(self, dateMin, budget)
            currentInt = self.targetCore.getNextFreeMemoryIntervalWhole(tempInt.getStart(), tempInt.getSize(), pages)
            if currentInt.getInterval().getStart()!=tempInt.getStart():
                found = False
                dateMin = currentInt.getInterval().getStart()

            #comparison with the Tints already virtually reserved
            else:
                inter, nd = self.intersects(currentInt, Tints)
                if inter:
                    found = False
                    dateMin = nd
                    
        return currentInt

    def intersects(self, currentInt, Tints):
        """Does interval currentInt intersect any of the intervals in Tints
        in time ?  returns True/False, and if True, the maximum end
        value between the end date of the intersecting intervals.

        """
        inter = False
        date = 0
        ciDesc = currentInt.getInterval()
        for i in Tints:
            iDesc = i.getInterval()
            if iDesc.getStart()<ciDesc.getStart()<iDesc.getEnd() or ciDesc.getStart()<iDesc.getStart()<ciDesc.getEnd():
                inter = True
                date = max(max(date, iDesc.getEnd()), ciDesc.getEnd())
        return inter, date
        
    def getFreeIntervalWholeSynchronous(self, proc, dateMin, funIdx):
        """
        Get an interval that is free on both the dma order line and on the proc, after dateMin, for the current job
        """
        intervalO = None
        found = False
        while not found:
            found = True
            intervalO = self.getNextFreeIntervalWholeOrders(dateMin, self.getBudgetOrder(funIdx))
            intTemp = Processor.getNextFreeIntervalWhole(proc, intervalO.getStart(), intervalO.getSize()) # no pages needed
            if intTemp.getStart() != intervalO.getStart():
                dateMin = intTemp.getStart()
                found = False
        assert intervalO != None, "interval for acquisition order must be defined"
        return intervalO
    
    def isValidOrder(self, intervalOrder, intervalTransfer):
        """
        Starting with a valid co-schedule of DMA orders and DMA transfers, does the insertion of intervalOrder and intervalTransfer make the schedule invalid ?
        """
        prevOSlot = self.coreOrders.getSlotBefore(intervalOrder.getStart())
        #nextOSlot = self.coreOrders.getSlotAfter(intervalOrder.getEnd())

        prevTSlot = self.getSlotBefore(intervalTransfer.getInterval().getStart())
        # nextTSlot = self.getSlotAfter(intervalTransfer.getEnd())
        if prevOSlot == None or prevTSlot == None:
            #First order and transfer
            return prevOSlot == None and prevTSlot == None

        # Otherwise:
        # There must be no idle time on the DMA between the order and the transfer
        test = Processor.getNextFreeIntervalWhole(self, intervalOrder.getEnd(), 1)
        if test.getStart() < intervalTransfer.getInterval().getStart():
            return False
        # Also: there must be a correspondance between the previous order and the previous transfer
        return (prevOSlot.getVal().getAcq() == prevTSlot.getVal().getAcq()) and (prevOSlot.getVal().getProcIdx()==prevTSlot.getVal().getProcIdx()) and (prevOSlot.getVal().getTaskIdx()==prevTSlot.getVal().getTaskIdx())

class DMAProc(Processor):
    """A processor with a paginated local memory (e.g. scratchpad), which
    stores a transfer rate (either from the processor making the copy
    itself, or the transfer rate of the used DMA, depending on the
    setting). In this model, the used memory pages (addresses) must be
    specified in lists.

    """
    def __init__(self, idx, nbPages, processorKind, transferRate):
        Processor.__init__(self, idx, processorKind)
        self.localMem = MemoryTimeIntervalList(nbPages)
        self.orderBudgets = 1 #For now, constant time for order (writing a source address, a destination address and a size. Must be adapted for each architecture
        self.transferRate = transferRate

    def getNbPages(self):
        return self.localMem.getNbPages()
    
    def getTransferBudget(self, size):
        return size*self.transferRate
        
    def getOrderBudgets(self, funIdx):
        return self.orderBudgets
    
    def getLocalMem(self):
        return self.localMem

    def getNextFreeMemoryInterval(self, start, size, pages):
        return self.localMem.getNextFreeInterval(start, size, pages)

    def getNextFreeMemoryIntervalWhole(self, start, size, pages):
        return self.localMem.getNextFreeIntervalWhole(start, size, pages)
    
    def getNextFreeInterval(self, dateMin, budget, pages):
        currentInt = None
        found = False
        while not found:
            found = True
            tempInt = Processor.getNextFreeInterval(self, dateMin, budget)
            currentInt = self.localMem.getNextFreeInterval(tempInt.getStart(), tempInt.getSize(), pages)
            if currentInt.getInterval().getStart()!=tempInt.getStart():
                found = False
                dateMin = currentInt.getInterval().getStart()

        return currentInt

    def getNextFreeIntervalWhole(self, dateMin, budget, pages = []):

        currentInt = None
        found = False
        while not found:
            found = True
            tempInt = Processor.getNextFreeIntervalWhole(self, dateMin, budget)
           
            currentInt = self.localMem.getNextFreeIntervalWhole(tempInt.getStart(), tempInt.getSize(), pages)
            if currentInt.getInterval().getStart()!=tempInt.getStart():
                found = False
                dateMin = currentInt.getInterval().getStart()

        return currentInt
        
    def removeFreeInterval(self, date, size, pages = None):
        Processor.removeFreeInterval(self, date, size)
        self.localMem.removeFreeInterval(date, size, pages)
        
    def updateSchedule(self, timeSlot):
        
        Processor.updateSchedule(self, timeSlot)

    def updateLocalMem(self, start, size, pages):
       
        self.localMem.removeFreeInterval(start, size, pages)

class DMAProcI2D(DMAProc):
    """
    A DMAProc with an improved I2DMem representation of memory intervals. In this model the memory spaces are specified by a start address and a size.
    """
    def __init__(self, idx, nbPages, processorKind, transferRate, startMemAddress=0):
        DMAProc.__init__(self, idx, nbPages, processorKind, transferRate)
        self.localMem= I2DMemList(startY=startMemAddress, sizeY=nbPages)
        self.nbPages = nbPages
        
    def getNbPages(self):
        return self.nbPages

    # def getNextFreeInterval(self, dateMin, budget, pages = None, startPage = None):
    #     i = None
    #     if pages == None:
    #         # No consideration of memory
    #         i = Processor.getNextFreeInterval(self, dateMin, budget)
    #     else:
    #         found = False
    #         while not found:
    #             i = Processor.getNextFreeInterval(self, dateMin, budget)
    #             i1,r = self.localMem.getNextFreeInterval(i.getStart(), i.getSize(), pages, startPage)
    #             if i1.getStart() == i.getStart():
    #                 found = True
    #     return i

    # def getNextFreeIntervalWhole(self, dateMin, budget, pages = None, startPage = None):
    #     i = None
    #     if pages == None:
    #         # No consideration of memory
    #         i = Processor.getNextFreeIntervalWhole(self, dateMin, budget)
    #     else:
    #         found = False
    #         while not found:
    #             i = Processor.getNextFreeIntervalWhole(self, dateMin, budget)
    #             i1,r = self.localMem.getNextFreeIntervalWhole(i.getStart(), budget, pages, startPage)
    #             if i1.getStart() == i.getStart():
    #                 found = True
    #         return i

    # def removeFreeMemoryInterval(self, date, size, startPage, pages):
    #     self.localMem.removeFreeInterval(date, size, startPage, pages)

    # def updateScheduleAndMemory(self, timeSlot, startPage, pages=None):
    #     self.updateSchedule(timeSlot)
    #     if pages != None:
    #         self.removeFreeMemoryInterval(timeSlot.getStart(), timeSlot.getBudgetWithIF(), startPage, pages)
        
        
class DMAArchi(Architecture):
    """
    A particular architecture where each processor should be a DMAProc including a local memory, and equiped with DMAs modelled as special Processors
    """
    def __init__(self, procs, dmas, scheduler=None):
        Architecture.__init__(self, procs, scheduler)
        self.interconnect = Interconnect(dmas)


    def __str__(self):
        res= Architecture.__str__(self)
        for dma in self.interconnect.getResources():
            res+=str(dma)+"\n\n"
        return res
    
    def getInterconnect(self):
        return self.interconnect

    def buildNew(self, nProcs, scheduler):
        assert False, "TODO: implement"
    #TODO: There may be a modelisation problem -- a conflict with the interconnect in archInterference
##############################################
# Tasks
##############################################

class DMAMonolithicFunction(MonolithicFunction):
    """A monolithic function in an architecture with DMA
    capabilities. Must specify the size of transfers for Read and
    Write phases, for each processor type (integer list).  A negative
    transfer size means it is not allowed, a transfer rate of 0 means
    no transfer is necessary (e.g. data/instructions are present locally).
    May also contain the memory "pages" the function occupies when loaded 
    on a processor kind

    """
    def __init__(self, idx, name, timeProfiles, memProfiles, acqSizes, restSizes, pages = None):
        """
        Memory profiles can be None
        """
        MonolithicFunction.__init__(self, idx, name, timeProfiles, memProfiles)
        self.transferSizes = []
        assert len(acqSizes) == len(restSizes), "There must be as many acq sizes as Restitution sizes"
        for i in range(len(acqSizes)):
            self.transferSizes.append((ExtendedInt(acqSizes[i], acqSizes[i]<0),ExtendedInt(restSizes[i], restSizes[i]<0)))
        self.pages = pages
        
    def getTransferSizes(self, procKind):
        assert procKind < len(self.transferSizes), "read and write sizes must be specified for each kind of processor"
        return self.transferSizes[procKind]

    def getMemPages(self, procKind):
        return self.pages[procKind]

##############################################
# Schedule entities
##############################################

class PageIntervalList(IntervalList):
    """
    Extension of IntervalList to allow removing intervals for particular pages in a paged memory
    """
    def __init__(self, interval=None):
        IntervalList.__init__(self, interval= interval)
        
    def removeFreeIntervalPages(self, date, size, MemTIL, page):
        """
        Removing a free interval in the context of paged memories (which are not part of a schedule)
        """
        currentNode=self
        while currentNode.getNext() != None and currentNode.getVal().endsBefore(date):
            currentNode=currentNode.getNext()

        res, remains = currentNode.getVal().cutFreeInterval(date, size)

        if len(remains)==2:
            newNode=PageIntervalList(remains[1])
            currentNode.insertBefore(newNode)

        if len(remains)>0:
            newNode=PageIntervalList(remains[0])
            currentNode.insertBefore(newNode)        
        
        ptr, isHead=currentNode.remove()
        if isHead:
            MemTIL.setFreeIntervals(ptr, page)
        return res



class MemoryTimeIntervalList():
    """
    A structure to represent reservation of memory pages in time.
    Uses PageIntervalLists, must implement some of the methods from IntervalList. Must be stored in the local memory object.
    """

    def __init__(self, nbPages, intervals=None):
        lists = []
        if intervals == None:
            for i in range(nbPages):
                lists.append(PageIntervalList())
        else:
            for i in range(nbPages):
                lists.append(PageIntervalList(intervals[i]))
        self.intervals = lists
        self.nbPages=nbPages

    def getNbPages(self):
        return self.nbPages
    
    def buildNew(self, intervals):
        return MemoryTimeIntervalList(self.nbPages,intervals)

    def setFreeIntervals(self, ptr, page):
        self.intervals[page]=ptr
        
    def getNextFreeInterval(self, date, size, pages):
        """
        Returns the first time interval that is free on all required pages after date and for at most size
        """
        currentInterval = self.intervals[pages[0]].getNextFreeInterval(date, size)
        found = False
        while not found:
            found = True
            for i in pages:
                temp = self.intervals[i].getNextFreeInterval(currentInterval.getStart(), currentInterval.getSize())
                if temp.getStart() > currentInterval.getStart():
                    currentInterval = temp
                    found = False
        return IntervalMem(currentInterval, pages)

    def getNextFreeIntervalWhole(self, date, size, pages):
        """
        Returns the first time interval that is free on all required pages after date and for at exactly size
        """
        currentInterval = self.intervals[pages[0]].getNextFreeIntervalWhole(date, size)
        found = False
        while not found:
            found = True
            for i in pages:
                temp = self.intervals[i].getNextFreeIntervalWhole(currentInterval.getStart(), size)
                if temp.getStart() > currentInterval.getStart():
                    currentInterval = temp
                    found = False
        return IntervalMem(currentInterval, pages)

    def removeFreeInterval(self, date, size, pages):
        """
        Removes free interval starting at date (must be called on the right date !) of size (must be called with the right size !) on the relevant pages.
        Returns the interval from the last page involved (but it has the same temporal properties as the others), though for now it is discarded at the caller level.
        """
        res = None
        for i in pages:
            res = self.intervals[i].removeFreeIntervalPages(date, size, self, i) 
            
        return res

class DMACompTime(CompTime): #Add the pages ?
    def __init__(self, date, budget, job, taskIdx, procIdx, dmaIdx, acq, ifBudget = 0):
        CompTime.__init__(self, date, budget, job, taskIdx, procIdx, ifBudget = 0)
        self.dmaIdx = dmaIdx
        self.acq = acq #True if acquisition, False if Restitution
        self.specialFun = True
        
    def __str__(self):
        res = "Proc: " + str(self.procIdx) + " " + CompTime.__str__(self)
        if self.acq:
            return "Acquisition for " + res
        else:
            return "Restitution for " + res

    def getAcq(self):
        return self.acq



class DMAOrderCompTime(DMACompTime): #Add the pages ?
    """
    Same as DMACompTime with overloaded __str__ method
    """
    def __str__(self):
        res = CompTime.__str__(self)
        res += " DMA "+ str(self.dmaIdx) + ": order="
        if self.acq:
            res += " Read from memory"
        else:
            res += " Write to memory"
        return res




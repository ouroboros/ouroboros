from aer import *

####################################################################################################
#                                                                                                  #
#                  Ouroboros module for basic REW (DMA-based PREM) scheduling                      #
#                                                                                                  #
#                                   @author: T. Carle                                              #
#                                                                                                  #
####################################################################################################

#----------------------------------------#
# Classes for description of REW setting #
#                                        #
# We consider a multicore with local     #
# memories to each core which can be     #
# loaded using a DMA or any              #
# equivalent mechanism. The shared       #
# memory is divided into pages of equal  #
# size which can be loaded in the local  #
# memories, with a direct mapping        #
# (associativity of 1) model             #
#                                        #
# We assume that the DMA has a           #
# potentially infinitely long, yet       #
# ordered queue of transfers to process  #
#----------------------------------------#

#TODO: Optim : sending data on all processor targets (not just to main memory)

#TODO: Suppress W phases when all successors are on the same processor.


####################################################
# Scheduler
####################################################

class REW_DF_Monolithic(DataflowMonolithic):
    def __init__(self, tasks, archi, funs):
        DataflowMonolithic.__init__(self, tasks, archi, funs)
        
#######################
# Fine grain scheduling
#######################

    def schedASAPMulticore(self, proc, job, budget, preemptive, interconnect, sDate = 0):
        """
        Schedules the job as 3 parts A, E and R as soon as
        possible. budget corresponds to the Exec portion WCET. the A
        and R budgets must be retrieved directly in the DMA(s).

        """
        dateMin = job.getReadyDateMulticore()
        funIdx = self.tasks[job.getTaskIdx()].getFunIdx()
        pages = self.functions[funIdx].getMemPages(proc.getProcKind())
        if len(interconnect.getResources()) > 0:
            dma = interconnect.getResources()[0] # For now we only work with one DMA
            return (self.schedASAPMulticoreOneDMA(proc, budget.getBudget()[0], preemptive, dateMin, dma, pages, funIdx), dma.getIdx())
        assert False, "REW requires a dma"

    def getFreeIntervalWholeSynchronous(self, dma, proc, dateMin, funIdx):
        """
        Get an interval that is free on both the dma order line and on the proc, after dateMin, for the current job
        """
        return dma.getFreeIntervalWholeSynchronous(proc, dateMin, funIdx)

    def getIntervalsWholeDMATransfer(self, dma, proc, dateMin, budgetTransfer, jobPages, funIdx, orders = True):
        """
        Get an interval for DMA order, and the corresponding one for DMA transfer, making sure they correspond to a schedule that can be programmed
        """

        # If we do not want to explicitly represent the programming of the DMA by the cores
        if not orders:
            return None, dma.getNextFreeIntervalWhole(dateMin, budgetTransfer, jobPages)

        # If we want to represent explicitely the programming of the DMA by the cores
        intervalOrder = self.getFreeIntervalWholeSynchronous(dma, proc, dateMin, funIdx)
        intervalTransfer = dma.getNextFreeIntervalWhole(intervalOrder.getEnd(), budgetTransfer, jobPages)
        while not dma.isValidOrder(intervalOrder, intervalTransfer):
            intervalOrder = self.getFreeIntervalWholeSynchronous(dma, proc, intervalTransfer.getInterval().getStart(), funIdx)
            intervalTransfer = dma.getNextFreeIntervalWhole(intervalOrder.getEnd(), budgetTransfer, jobPages)

        return intervalOrder, intervalTransfer

    
    def schedASAPMulticoreOneDMA(self, proc, budget, preemptive, dateMin, dma, jobPages, funIdx):
        """
        Main scheduling engine, using the specified DMA
        """

        #Step 0: Configure DMA with target core
        dma.setTargetCore(proc)

        function = self.functions[funIdx]
        sizeA, sizeR = function.getTransferSizes(proc.getProcKind())
        (budgetA, budgetR) = dma.getTransferBudget(sizeA), dma.getTransferBudget(sizeR) 


        if budgetA.isInfinite():
            #If Acquisition budget is infinite, it is impossible to schedule to this core
            return [None, None, None, None, Interval(0, ExtendedInt(0,True))]
        
        # Acquisition is order plus transfer
        intervalOA = None
        intervalA = None
        
        intervalE = None

        # Restitution is order plus transfer
        intervalOR = None
        intervalR = None

        found = False
        while not found:
            found = True
            #Step 1: find a slot for Acquisition on DMA
            #if not budgetA.isInfinite():
            if not budgetA==0:
                intervalOA, intervalA = self.getIntervalsWholeDMATransfer(dma, proc, dateMin, budgetA, jobPages, funIdx)
                dateMin = intervalA.getInterval().getEnd()
                jobPages = intervalA.getPages()
            #Step 2: find a slot for Execution on proc
            if preemptive:
                assert False, "TODO"
            else:
                intervalE = proc.getNextFreeIntervalWhole(dateMin, budget, jobPages)
                jobPages = intervalE.getPages()
                
            #Step 3: find a slot for Restitution on DMA
            #if not budgetR.isInfinite():
            if not budgetR==0:
                intervalOR, intervalR = self.getIntervalsWholeDMATransfer(dma, proc, intervalE.getInterval().getEnd(), budgetR, jobPages, funIdx)
            #Step 4: verify that memory pages can be reserved without interruption from the start of A till the end of R

            #if (not budgetA.isInfinite()) and (not budgetR.isInfinite()):
            if (not budgetA==0) and (not budgetR==0):
                intervalMem = proc.getLocalMem().getNextFreeIntervalWhole(intervalA.getInterval().getStart(), intervalR.getInterval().getEnd()-intervalA.getInterval().getStart(), jobPages)
                if intervalMem.getInterval().getStart()!=intervalA.getInterval().getStart():
                    found = False
                    dateMin = intervalA.getInterval().getEnd() # This could be optimized TODO
            #elif (not budgetA.isInfinite()):
            elif (not budgetA==0):
                intervalMem = proc.getLocalMem().getNextFreeIntervalWhole(intervalA.getInterval().getStart(), intervalE.getInterval().getEnd()-intervalA.getInterval().getStart(), jobPages)
                if intervalMem.getInterval().getStart()!=intervalA.getInterval().getStart():
                    found = False
                    dateMin = intervalA.getInterval().getEnd() # This could be optimized TODO
            #elif (not budgetR.isInfinite()):
            elif (not budgetR==0):
                intervalMem = proc.getLocalMem().getNextFreeIntervalWhole(intervalE.getInterval().getStart(), intervalR.getInterval().getEnd()-intervalE.getInterval().getStart(), jobPages)
                if intervalMem.getInterval().getStart()!=intervalE.getInterval().getStart():
                    found = False
                    dateMin = min(intervalE.getInterval().getEnd(), intervalMem.getInterval().getStart()) # This could be optimized TODO
            else:
                intervalMem = proc.getLocalMem().getNextFreeIntervalWhole(intervalE.getInterval().getStart(), intervalE.getInterval().getEnd()-intervalE.getInterval().getStart(), jobPages)
                if intervalMem.getInterval().getStart()!=intervalE.getInterval().getStart():
                    found = False
                    dateMin = min(intervalE.getInterval().getEnd(), intervalMem.getInterval().getStart()) # This could be optimized TODO
            
        #Final step: De-configure DMA to avoid bugs
        dma.setTargetCore(None)
        return ([intervalOA, intervalA, intervalE, intervalOR, intervalR], intervalMem.getPages())

#######################
# Coarse grain scheduling
#######################
        
    def scheduleJobNonPreemptiveMulticore(self, readyJob):
        res=None
        # Determine which mapping minimises the end date of the job
        funIdx = self.tasks[readyJob.getTaskIdx()].getFunIdx()
        for p in self.archi.getProcs():
            budget=self.getBudget(p.getIdx(), readyJob.getTaskIdx())
            if(not budget.isInfinite()):
                candIntervals, dmaIdx = self.schedASAPNonPreemptiveMulticore(p, readyJob, budget, self.archi.getInterconnect())
                res2 = AERJobSchedulingResult(candIntervals, p.getIdx(), dmaIdx)#, self.functions[funIdx].getMemPages(p.getProcKind()))
                if not res2.getEnd().isInfinite() and(res==None or (res.getEnd()>res2.getEnd())):
                    res = res2
        return res

    def scheduleJobPreemptiveMulticore(self, readyJob):
        """
        Not implemented for now
        """
        assert False, "TODO"
        
    def applySchedulingDecision(self, readyJob, result):
 
        #Step 0: on job
        readyJob.setPages(result.getPages())
        
        #Step 1: on processor schedule
        Scheduler.applySchedulingDecision(self, readyJob, result)
        
        #Step 2: on processor memory allocation
        dmaInt = result.getDmaIntervals()
        start = None
        if dmaInt[0] != None:
            start = dmaInt[0].getInterval().getStart()
        else:
            start = result.getIntervals()[0].getStart()
        end = None
        if dmaInt[1] != None:
            end = dmaInt[1].getInterval().getEnd()
        else:
            end = result.getIntervals()[-1].getEnd()
            
        self.archi.getProcs()[result.getProcIdx()].updateLocalMem(start, end-start, result.getPages())

        #Step 3: on DMA schedule
        dma = self.archi.getInterconnect().getResources()[result.getDmaIdx()]
        if dmaInt[0] != None:
            tsA = DMACompTime(dmaInt[0].getInterval().getStart(), dmaInt[0].getInterval().getSize(), readyJob, readyJob.getTaskIdx(), result.getProcIdx(), dma.getIdx(), True)
            readyJob.addASlot(tsA)
            dma.updateSchedule(tsA)
        if dmaInt[1] != None:
            tsR = DMACompTime(dmaInt[1].getInterval().getStart(), dmaInt[1].getInterval().getSize(), readyJob, readyJob.getTaskIdx(), result.getProcIdx(), dma.getIdx(), False)
            readyJob.addRSlot(tsR)
            dma.updateSchedule(tsR)

        #Step 4: apply the orders on both the processor and the DMA
        dmaOInt = result.getDmaOrderIntervals()

        #Step 4a: on the processor and step 4b: on the DMA
        if dmaOInt[0] != None:
            tSlot = DMAOrderCompTime(dmaOInt[0].getStart(), dmaOInt[0].getSize(), readyJob, readyJob.getTaskIdx(), result.getProcIdx(), dma.getIdx(), True) # Acquisition order
            readyJob.addTimeSlot(tSlot)
            self.archi.getProcs()[result.getProcIdx()].updateSchedule(tSlot)
            dmaAOSlot = DMACompTime(dmaOInt[0].getStart(), dmaOInt[0].getSize(), readyJob, readyJob.getTaskIdx(), result.getProcIdx(), dma.getIdx(), True)
            dma.updateScheduleOrders(dmaAOSlot)
            
        if dmaOInt[1] != None:
            tSlot = DMAOrderCompTime(dmaOInt[1].getStart(), dmaOInt[1].getSize(), readyJob, readyJob.getTaskIdx(), result.getProcIdx(), dma.getIdx(), False) # Restitution order
            readyJob.addTimeSlot(tSlot)
            self.archi.getProcs()[result.getProcIdx()].updateSchedule(tSlot)
            dmaROSlot = DMACompTime(dmaOInt[1].getStart(), dmaOInt[1].getSize(), readyJob, readyJob.getTaskIdx(), result.getProcIdx(), dma.getIdx(), False)
            dma.updateScheduleOrders(dmaROSlot)

        
class REW_unfixed_DF_Monolithic(REW_DF_Monolithic):
    """
    A particular case of REW where the pages for tasks are not fixed in advanced, but determined during the scheduling. However, it is known in advance how many adjacent pages must be allocated for each function on each processor kind.
    """
#######################
# Fine grain scheduling
#######################

    def schedASAPMulticore(self, proc, job, budget, preemptive, interconnect, sDate = 0):
        """
        Schedules the job as 3 parts A, E and R as soon as
        possible. budget corresponds to the Exec portion WCET. the A
        and R budgets must be retrieved directly in the DMA(s).

        """
        dateMin = job.getReadyDateMulticore()
        funIdx = self.tasks[job.getTaskIdx()].getFunIdx()
        pages = self.functions[funIdx].getMemPages(proc.getProcKind()) # In this model, pages is an integer
        if len(interconnect.getResources()) > 0:
            dma = interconnect.getResources()[0] # For now we only work with one DMA
            local_results = []
            for i in range(proc.getNbPages()-pages): #Sliding windows of size pages (memory is not used as a circular buffer in this model)
                local_pages = []
                for j in range(pages):
                    local_pages.append(i+j)
                local_results.append((self.schedASAPMulticoreOneDMA(proc, budget.getBudget()[0], preemptive, dateMin, dma, local_pages, funIdx), local_pages))
            return (self.bestResult(local_results), dma.getIdx())
        assert False, "REW requires a DMA"

    def scheduleJobNonPreemptiveMulticore(self, readyJob):
        res=None
        # Determine which mapping minimises the end date of the job
        funIdx = self.tasks[readyJob.getTaskIdx()].getFunIdx()
        for p in self.archi.getProcs():
            budget=self.getBudget(p.getIdx(), readyJob.getTaskIdx())
            if(not budget.isInfinite()):
                (candIntervals, pages), dmaIdx = self.schedASAPNonPreemptiveMulticore(p, readyJob, budget, self.archi.getInterconnect())
                res2 = AERJobSchedulingResult(candIntervals, p.getIdx(), dmaIdx, pages)
                if not res2.getEnd().isInfinite() and(res==None or (res.getEnd()>res2.getEnd())):
                    res = res2
        return res

        
    def bestResult(self, res_list):
        """
        Locally, we choose the solution which reduces the reservation duration of the memory pages. At the global level, we will still elect the ASAP solution.
        """
        res = None
        pages = None 
        duration = ExtendedInt(0, True) #we will minimize duration
        for (r,l) in res_list:
            if not r[4]==None: 
                d = r[4].getEnd()
            elif not r[2]==None:
                d = r[2].getEnd()
            else:
                assert False, "Error with the reservations (1)"
            if not r[0]==None:
                d -= r[0].getStart()
            elif not r[2]==None:
                d -= r[2].getStart()
            else:
                assert False, "Error with the reservations (2)"

            if duration > d:
                duration = d
                res = r
                pages = l
        return res, pages

# class REW_I2D_DF_Unfixed(REW_unfixed_DF_Monolithic):
#     def schedASAPMulticore(self, proc, job, budget, preemptive, interconnect):
#         """Makes use of the I2D representation of memory reservation
#         intervals. TODO: requires some overloading to get an optimized
#         algo to determine memory address zones which work, using the 2D interval primitives.

#         """
#         dateMin = job.getReadyDateMulticore()
#         funIdx = self.tasks[job.getTaskIdx()].getFunIdx()
#         pages = self.functions[funIdx].getMemPages(proc.getProcKind()) # In this model, pages is an integer
#         if len(interconnect.getResources()) > 0:
#             dma = interconnect.getResources()[0] # For now we only work with one DMA
#             local_results = (self.schedASAPMulticoreOneDMA(proc, budget.getBudget()[0], preemptive, dateMin, dma, local_pages, funIdx))
#             return (local_results, dma.getIdx())
#         assert False, "TODO: implement AER without DMA a.k.a. pure AER -> check schedASAPMulticoreAERNoDMA"

#     def scheduleJobNonPreemptiveMulticore(self, readyJob):
#         res=None
#         # Determine which mapping minimises the end date of the job
#         funIdx = self.tasks[readyJob.getTaskIdx()].getFunIdx()
#         for p in self.archi.getProcs():
#             budget=self.getBudget(p.getIdx(), readyJob.getTaskIdx())
#             if(not budget.isInfinite()):
#                 (candIntervals, pages), dmaIdx = self.schedASAPNonPreemptiveMulticore(p, readyJob, budget, self.archi.getInterconnect())
#                 res2 = AERJobSchedulingResult(candIntervals, p.getIdx(), dmaIdx, pages)
#                 if not res2.getEnd().isInfinite() and(res==None or (res.getEnd()>res2.getEnd())):
#                     res = res2
#         return res

#####################################################
# Test
#####################################################

# !!!! Model where pages are specified: works with REW_DF_Monolithic scheduler
#f0 = DMAMonolithicFunction(0, "f0", [2, 5], [[], []], [0, 2], [0, 2], [[0, 1],[2]]) # Idx, name, durations on processor kinds, memory access profiles, acq sizes, rest sizes, memory "pages"
#f1 = DMAMonolithicFunction(1, "f1", [1, 1], [[], []], [1, 1], [1, 1], [[2],[0]])
#f2 = DMAMonolithicFunction(2, "f2", [5, 3], [[], []], [5, 3], [2, 2], [[0],[1]])
#f3 = DMAMonolithicFunction(3, "f3", [2, 2], [[], []], [2, 0], [1, 0], [[1],[2]])

# !!!! Model where pages are unspecified, but number of adjacent pages is specified: works with REW_unfixed_DF_Monolithic scheduler
f0 = DMAMonolithicFunction(0, "f0", [2, 5], [[], []], [0, 2], [0, 2], [(2, 0), (1,0)]) # Idx, name, durations on processor kinds, memory access profiles, acq sizes, rest sizes, number of memory "pages"
f1 = DMAMonolithicFunction(1, "f1", [1, 1], [[], []], [1, 1], [1, 1], [(1,0), (1,0)])
f2 = DMAMonolithicFunction(2, "f2", [5, 3], [[], []], [5, 3], [2, 2], [(1,0), (1,0)])
f3 = DMAMonolithicFunction(3, "f3", [2, 2], [[], []], [2, 0], [1, 0], [(1,0), (1,0)])

functions = [f0, f1, f2, f3]

n0 = AERDFNode("n0", 0, 0, [])
n1 = AERDFNode("n1", 1, 1, [])
n2 = AERDFNode("n2", 2, 2, [Predecessor(0)])
n3 = AERDFNode("n3", 3, 3, [])
tasks = [n0, n1, n2, n3]

p0 = DMAProcI2D(0, 3, 0, 1)
p1 = DMAProcI2D(1, 3, 1, 1)
processors = [p0, p1]

dma = DMA(0, 1)# Idx, transferRate

archi = DMAArchi(processors, [dma])

#sched = AER_DF_Monolithic(tasks, archi, functions)
sched = REW_DF_Monolithic(tasks, archi, functions)
sched.schedule()
print(sched)

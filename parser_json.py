import argparse
import json
import os
import os.path
import sys
import xml.etree.ElementTree

from tkinter import *
from tkinter import filedialog

from architectures import *
from tasks import *
from tasks import DataflowNode
from tasks import PeriodicTask

from functions import *

from scheduler import *

#from boundedInterference import BoundedDFNode
#from dma import DMADFNode
#from aer import AERDFNode

class MetaTask(Task):
    def __init__(self, name, idx, funIdx, preds, period, deadline, migration, tolerancePerc, pages):
        Task.__init__(self, name, idx, funIdx, preds)

        #For periodic tasks
        self.period = period
        self.deadline = deadline
        self.migration = migration

        #For BoundedInt tasks
        self.tolerancePerc = tolerancePerc

        #For DMA tasks
        self.pages = pages

    def makeDataFlowNode(self):
        assert self.name != None and self.idx != None and self.funIdx != None and self.preds != None, "Dataflow Node misspecified"
        return DataflowNode(self.name, self.idx, self.funIdx, self.preds)

    def makePeriodicTask(self):
        assert self.name != None and self.idx != None and self.funIdx != None and self.period != None and self.deadline != None and self.migration != None, "Periodic task misspecified"
        return PeriodicTask(self.name, self.idx, self.funIdx, self.period, self.deadline, self.migration)

    def makeBoundedDFNode(self):
        assert self.name != None and self.idx != None and self.funIdx != None and self.preds != None and self.tolerancePerc != None, " Bounded Dataflow Node misspecified" 
        return BoundedDFNode(self.name, self.idx, self.funIdx, self.preds, self.tolerancePerc)

 #   def makeDMADFNode(self):
 #       assert(self.name != None and self.idx != None and self.preds != None and self.pages != None, " DMA Dataflow Node misspecified")
 #       return DMADFNode(self.name, self.idx, self.preds, self.pages)

    def makeAERDFNode(self):
        assert self.name != None and self.idx != None and self.funIdx != None and self.preds != None and self.pages != None, " AER Dataflow Node misspecified" 
        return AERDFNode(self.name, self.idx, self.preds, self.pages)

    
# load the task descriptions
def apploader_tasks(path):
    app = json.load(open(path))
    print("Analysing tasks...")
    tasks = []
    for t in app:
        preds = []
        if "preds" in t:
            for p in t["preds"]:
                preds.append(Predecessor(p))

        period = None
        if "period" in t:
            period = t["period"]

        deadline = None
        if "deadline" in t:
            deadline = t["deadline"]
        else:
            deadline = period
        
        migration = False
        if "migration" in t:
            if t["migration"] in ["True", "true", "TRUE"]:
                migration = True
            
        tolerance = None
        if "tolerancePerc" in t:
            tolerance = t["tolerancePerc"]
            
        pages = None
        if "pages" in t:
            pages = t["pages"]
            
        tasks.append(MetaTask(t["name"], t["idx"], t["funIdx"], preds, period, deadline, migration, tolerance, pages))
    print("\tDone.")
    print("\t"+str(len(tasks))+ " tasks loaded.")

    tasks.sort(key=lambda x: x.idx)
    
    return tasks

def gui_apploader_task():
    i_file = filedialog.askopenfilename(title="Open a task description file",filetypes=[('json files','.json'),('all files','.*')])
    return i_file

# load the architecture description
def apploader_arch(path):
    arch = json.load(open(path))
    print("Analysing processors...")
    procs = []
    for p in arch["procs"]:
        procs.append(Processor(p["idx"], p["kind"]))
    print("\tDone.")
    print("\t"+str(len(procs))+ " processors loaded.")

    procs.sort(key=lambda x: x.idx)
    print("Analysing DMAs...")
    dmas=[]
    if "dma" in arch:
        for d in arch["dma"]:
            dmas.append(DMA(d["idx"], d["transferRate"]))
    print("\tDone.")
    print("\t"+str(len(dmas))+ " dmas loaded.")
            
    
    return procs, dmas

def gui_apploader_arch():
    i_file = filedialog.askopenfilename(title="Open an architecture description file",filetypes=[('json files','.json'),('all files','.*')])
    return i_file

# load the functions description
def apploader_fun(path):
    data = json.load(open(path))
    print("Analysing functions...")
    funs = []
    for f in data:
        budgets = f["durations"]
        funs.append(MonolithicFunction(f["idx"], f["name"], budgets))
    print("\tDone.")
    print("\t"+str(len(funs))+ " functions loaded.")

    funs.sort(key=lambda x: x.idx)
    
    return funs

def gui_apploader_fun():
    i_file = filedialog.askopenfilename(title="Open a function description file",filetypes=[('json files','.json'),('all files','.*')])
    return i_file


tasks = apploader_tasks("testTask.json")
procs, dmas = apploader_arch("testArchi.json")
funs = apploader_fun("testFun.json")


tasks2=[]
for t in tasks:
    tasks2.append(t.makeDataFlowNode())
arch = Architecture(procs)



s = DataflowMonolithic(tasks2, arch, funs)
s.schedule()
print(s)

from scheduler import *
from archInterference import *
####################################################################################################
#                                                                                                  #
#                         Ouroboros module for scheduling with bounded interference                #
#                                                                                                  #
#                                   @author: T. Carle                                              #
#                                                                                                  #
####################################################################################################

#----------------------------------------#
# Classes to describe tasks as a WCET    #
# plus a fixed margin for interference.  #
# Once the system is scheduled, we       #
# compute the interference corresponding #
# to this schedule: each task has a      #
# margin to take interference without    #
# needing to be rescheduled.             #
#----------------------------------------#

##############################################
# Architecture
##############################################

class BoundedMultiCoreBus(GenericMulticoreWithBus):
    """
    We consider that the extra budgets for interference tolerance are already accounted for in the specified budgets for tasks on the processors.
    """
    def buildNew(self, nProcs, nInterconnect, scheduler):
        nArchi = self.archi.buildNew(nProcs, scheduler)
        return BoundedMulticoreBus(nArchi, nInterconnect)

    def applyInterferences(self, date):
        """
        Optimized version (no cloning)
        """
        #TODO: this will not be extended easily to a mixed model (first bounded, then extension of slots)
        procs = self.getProcs()
        for p in procs:
            ts = p.getSchedule()
            if ts.getValue()==None:
                ts = ts.getNext()
                while ts != None:
                    penalty = self.computeInterferenceForSlot(ts, Interval(ts.getStart(), ts.getBudget()), procs)
                    ts.increaseIFBudget(penalty)
                    ts = ts.getNext()

    def applyAwayPenalty(self, srcProc, slot, awayPenalty):
        """
         Overload: in the bounded interference model, the slot interference counter is incremented and checked. Then we call an update function to remove the corresponding free intervals.
         """
        slot.increaseIFBudget(awayPenalty)
         
##############################################
# Tasks
##############################################

class BoundedDFNode(DataflowNode):
    """
    A dataflow node with extra budget for potential interference
    """

    def __init__(self, name, idx, funIdx, preds, percentage):
        DataflowNode.__init__(self, name, idx, funIdx, preds)
        self.percentage = percentage # percentage of extra budget to account for interference

    def generateJobs(self, datesAndProcs):
        self.jobs.append(BoundedJob(0, self.idx, 1, datesAndProcs, self.percentage))

class BoundedJob(Job):
    """
    A job that can accept a bounded quantity of interference
    """
    def __init__(self, idx, taskIdx, nbJobsOfTask, datesAndProcs, percentage):
        Job.__init__(self, idx, taskIdx, nbJobsOfTask, datesAndProcs)
        self.percentage = percentage # used to compute the maxIFbudget when the BoundedSlot is created
        self.tolerance = None # To be set once the job has been scheduled

    def getPercentage(self):
        return self.percentage

    def setTolerance(self, tolerance):
        self.tolerance = tolerance

##############################################
# Scheduling entities
##############################################

class BoundedCompTime(CompTime):
    """
    Reserved time slots accounting with bounded budget to tolerate interference, 
    and mechanisms to detect interference overload. Pretty much the same as normal 
    comptime slots, but with a test for tolerance exhaustion.
    The total tolerance for the job is indicated in the 
    job itself
    """
    def __init__(self, date, budget, job, taskIdx, procIdx, ifBudget = 0):
        CompTime.__init__(self, date, budget, job, taskIdx, procIdx, ifBudget)
        
    def increaseIFBudget(self, penalty):
        CompTime.increaseIFBudget(self, penalty)
        
        sumPenalty = 0
        for ts in self.job.getTimeSlots():
            sumPenalty += ts.getIFBudget()
            
        assert sumPenalty <= self.job.getTolerance(), "Task: " + str(self.getTaskIdx()) + " Job: "+ str(self.job.getIdx()) + " has exceeded it tolerance (" + str(self.job.getTolerance()) + ") to interference"

    def __str__(self):
        return "task: " + str(self.taskIdx) + ", job: " + str(self.job.getIdx()) + " start: " + str(self.date) + " budget: " + str(self.getBudget()) + " consumed tolerance: " + str(self.getIFBudget()) + " over total tolerance: " + str(self.job.getTolerance())
    
    def clone(self):
        return BoundedCompTime( self.date, self.budget, self.job, self.taskIdx, self.procIdx, self.ifBudget)
    
##############################################
# Scheduler
##############################################

class BoundedDFMonolithic(DataflowMonolithic):
    """
    A class to schedule a dataflow graph with a monolithic task model and bounded interference.
    Pretty much the same as DataflowMonolithic, but using BoundedCompTime slots (thus taking 
    into account the maximum interference tolerance for each slot).
    """

    def createNewTimeSlot(start, size, job, procIdx):
        """
        Overload with BoundedCompTime slots
        """
        return BoundedCompTime(start, size, job, job.getTaskIdx(), procIdx)

    def applySchedulingDecision(self, readyJob, result):
        """Pretty much the same as the original one, but using
        BoundedCompTime, and computing the tolerance for the result.
        Since we make the assumption that the extra duration for
        interference tolerance is specified inside the budgets for
        tasks on procs, this extra duration is already accounted for
        in the result.
        """
        # Compute and apply tolerance:
        tolerance = self.getBudget(result.getProcIdx(), readyJob.getTaskIdx()) * readyJob.getPercentage()//100
        readyJob.setTolerance(tolerance)

        # Launch the classical one, but with the overloaded createNewTimeSlot
        Scheduler.applySchedulingDecision(self, readyJob, result)


from architectures import *

######################################################################
#                                                                    #
#             Ouroboros module for interference                      #
#                    modelling and analysis                          #
#             @author: T. Carle                                      #
#                                                                    #
######################################################################




#######################################################################
# Generic architecture classes for 
#######################################################################
class ArchInterference():
    """An addition to architecture classes for scheduling with interference. In
    these methods, we make no assumption on when the interference
    computation is made.TL;DR: if the complete schedule
    for all jobs is performed before the interference
    computation, this version is too heavy and we could design
    another, lighter class with no cloning. 

    Long version-> interference computation is always performed on a cloned version
    of the architecture. If all job scheduling is done before
    computing the interference, this is not necessary.
    """
    def __init__(self, archi, interconnect, scheduler = None):        
        self.archi = archi
        self.interconnect = interconnect

    def getProcs(self):
        return self.archi.getProcs()

    def getArchi(self):
        return self.archi

    def getScheduler(self):
        return self.archi.scheduler
    
    def setScheduler(self, scheduler):
        self.archi.setScheduler(scheduler)
    
    def setInterconnect(self, nInterconnect):
        self.interconnect = nInterconnect
        
    def cloneProcsUpTo(self, date):
        """Returns a list with clones of the processors of the architecture,
        with their schedules and interval lists "copied" up to date "date"

        """
        newProcs= []
        lastSlots=[] # A list of the last slot for each proc, which
                     # must be rescheduled, and for which the
                     # recomputation of interference must be
                     # performed, but must not affect the count of
                     # interference for the already scheduled slots.
        for proc in self.getProcs():
            np,ls=proc.cloneUpTo(date)
            newProcs.append(np)
            lastSlots.append(ls)
        
        return newProcs,lastSlots

    def getRemainderSlots(self, date):
        """Returns a list of linked lists (one for each processing resource)
        with the time slots starting after or at date, so they can be
        rescheduled with interference.

        """
        rest = []
        for proc in self.getProcs():
            rest.append(proc.getRemainderSlots(date))
        result = RemainLists(rest)
        return result

    def getNextSlot(self, remainderSlots):
        """Returns next slot to be rescheduled. This slot is the one with the
        minimum available date (inputs ready and processing resource
        ready-- with the assumption that after resource ready means
        there is nothing scheduled until the end of schedule).

        No longer supported: or in case there is multiple slots with
        minimal starting date, the one with the smallest end date.

        Returns None if all slots have been rescheduled

        """
        if remainderSlots.isEmpty():
            return None, None
        return remainderSlots.getMin(self.getProcs())
        
    def reScheduleSlot(self, slot, asapDate, cloneProcs):
        """Re-schedule a slot, with interference, on the same
        processor.

        """
        # Step 1: select the processing resource
        proc = cloneProcs[slot.getProcIdx()] # cloneProcs is the list of cloned processors, not a method
        # Re-computation of schedule is done so that after date
        # asapDate, there is no scheduled slot yet
        ival = proc.getNextFreeIntervalWhole(asapDate, slot.getBudget()) 
        # Step 2: compute interference for equivalent slot starting at new date and apply interference on interfering slots
        tempSlot = CompTime(ival.getStart(), ival.getSize(), slot.getJob(), slot.getTaskIdx(), slot.getProcIdx(), 0)
        tempSlot.setId(slot.getId())
        penalty = self.computeInterferenceForSlot(tempSlot, ival, cloneProcs) # virtual
        #penalty = self.computeInterferenceForSlot(slot, ival, cloneProcs) # virtual
        # Step 3: declare new slot and schedule it
        nSlot = CompTime(ival.getStart(), ival.getSize(), slot.getJob(), slot.getTaskIdx(), slot.getProcIdx(), penalty)
        nSlot.setId(slot.getId()) #TODO: insert this in the CompTime constructor with optional argument, if possible
        proc.updateSchedule(nSlot)

    def computeInterferenceForSlot(self, RescheduledSlot, interval, cloneProcs):
        """Virtual: this function is responsible for setting the count of
        interferences between cores for each architecture element
        (arbiters or inherited classes) where interference occur. It returns the
        timing penalty to be applied to the slot given as input, given
        the current configuration. It is also responsible for
        extending the duration of the interfered time slots on other
        cores.

        """
        return 0

    def cleanInterferenceEngines(remain):
        """
        clean all interference engines from arbiters in the architecture, keeping all job references, except the ones in the remain list
        """
        self.interconnect.cleanInterferenceEngines(remain.toList())


    def reschedLastSlots(self, lastSlots):# Careful here: all elements in the list can interfere with each other, and these interferences must be accounted for by both interfering elements. However, interferences with the slots that are already in the schedule at the beginning must only affect the slots in lastSlots. The last slots must be scheduled again, they are not in the schedules yet.
        for i in range(len(lastSlots)):
            if lastSlots[i] == None:#Trivial case for when we recompute the schedule from date 0
                continue

            #TODO: finish this for nominal case
            
    def preludeApplyInterference(self, date): #virtual
        """First step in applyInterferences. MUST return a list of procs to
        operate on, the remainderSlots, and the architecture to work
        on. Can be overloaded in legacy classes that do not require cloning.

        """
        nProcs,lastSlots = self.cloneProcsUpTo(date)
        
        remain = self.getRemainderSlots(date)
        
        nInterconnect = self.interconnect.clone(remain.toList()) # must include the cleaning of the IFEngines
        nArch = self.buildNew(nProcs, nInterconnect, self.getScheduler())
        self.getArchi().scheduler.setArchi(nArch)
        #nArch.cleanInterferenceEngines(remain)
        nArch.reschedLastSlots(lastSlots)
        
        return nProcs, remain, nArch

    def applyAwayPenalty(self, srcProc, slot, awayPenalty):
        """
        Virtual: how to apply the away penalty in computeInterferenceForSlot
        """
        srcProc.applyInt(slot, awayPenalty)
        
    def applyInterferences(self, date): #virtual
        """Given the current state of the schedule, create a new architecture,
        apply interference penalties, starting at date, keeping all
        mapping choices as they are.

        """
        nProcs, remain, nArch = self.preludeApplyInterference(date)
        nSlot, asapDate = nArch.getNextSlot(remain) # getNextSlot is responsible for maintaining the data structure
        while nSlot != None:
            nArch.reScheduleSlot(nSlot, asapDate, nProcs)
            nSlot, asapDate = nArch.getNextSlot(remain)
        return nArch

    def buildNew(self, nProcs, nInterconnect, scheduler):
        """
        Virtual: to be overloaded in each legacy class
        """
        nArchi = self.archi.buildNew(nProcs)
        return ArchInterference(nArchi, nInterconnect, scheduler)

class IFInterconnect(Interconnect):
    """
    Interconnect for problems where we want to perform interference analysis
    """
    def __init__(self, resources):
        Interconnect.__init__(self, resources)

    def cleanInterferenceEngines(self, remain):
        """clean all interference engines from arbiters in the interconnect,
        keeping all job references, except the ones in the remain list
        Careful: for now we consider that all resources in the arbiter
        have a method called cleanInterferenceEngine -- although it
        may do nothing. But we only implement it for Arbiters now
        because it is the only kind of resource for now.

        """
        for res in self.resources:
            res.cleanInterferenceEngine(remain)


    
class IFComputationEngine():
    """
    Support for interference computation in an arbiter
    """

    def __init__(self, nbSources):
        self.alreadyCounted={} # Dictionnary: for each job (idx),
                               # contains a list of size number of
                               # arbiter sources with for each source
                               # the number of interference already
                               # counted
        self.nbSources = nbSources
        
    def getAlreadyCounted(self, jobIdx, sourceIdx):
        if jobIdx in self.alreadyCounted:
            return self.alreadyCounted[jobIdx][sourceIdx]
        return 0

    def incrementCount(self, jobIdx, sourceIdx):
        if jobIdx not in self.alreadyCounted:
            self.alreadyCounted[jobIdx] = [0]*self.nbSources
        self.alreadyCounted[jobIdx][sourceIdx]+=1

    def clearCount(self, jobIdx):
        self.alreadyCounted[jobIdx] = [0]*self.nbSources

    def cloneCE(self, rejectedList=[]):
        """Clones a computation engine, forgetting about the jobIdx that are
        in the rejectedList (by default, no jobIdx is forgotten)

        """
        newCE = IFComputationEngine(self.nbSources)
        for jobIdx in self.alreadyCounted:
            if jobIdx not in rejectedList:
                newCE.alreadyCounted[jobIdx] = self.alreadyCounted[jobIdx]
        return newCE

    
class Arbiter():
    """A class to model shared resources as sequential or
    parallel. Arbiters and inherited classes are the support for
    interference computation: they must store the state of the
    interferences that are already taken into account

    """

    def __init__(self, idx, penalty, sources, destination=None):
        self.idx = idx
        self.penalty= penalty
        self.sources= sources
        self.destination= destination
        self.interferenceEngine=IFComputationEngine(len(sources))

    def getIdx(self):
        return self.idx
    
    def getPenalty(self):
        """
        Returns the unit value for a penalty on this arbiter. May be refined in the future for heterogeneous accesses.
        """
        return self.penalty

    def getSourceIdx(self, procIdx):
        """
        Translates a processor index into its corresponding source index for the arbiter
        """
        i=0
        while i<len(self.sources):
            if self.sources[i]==procIdx:
                return i
            i+=1
        assert 0==1 , "Source does not exist"

        
    def computePenalty(self, slot, rescheduledSlot, acc):
        """Returns the additional penalties (for the rescheduled slot and for
        the slot of index slotIdx) induced by one access made by
        rescheduledSlot in potential interference with slot
        """
        addr=acc.getAddress()
        #if slot.getAccesses()==None or rescheduledSlot.getAccesses()==None:
        if slot.getAccesses(addr)==None:
            return 0,0
        
        sAccs = slot.getAccesses(addr)
        
        #alreadyCounted = self.interferenceEngine.getAlreadyCounted(rescheduledSlot.getJob().getUniqueIdx(), self.getSourceIdx(slot.getProcIdx()))
        alreadyCounted = self.interferenceEngine.getAlreadyCounted(rescheduledSlot.getId(), self.getSourceIdx(slot.getProcIdx()))
        localPenalty = 0
        
        if alreadyCounted < rescheduledSlot.getAccesses(addr).getNumber():
            #self.interferenceEngine.incrementCount(rescheduledSlot.getJob().getUniqueIdx(), self.getSourceIdx(slot.getProcIdx()))
            self.interferenceEngine.incrementCount(rescheduledSlot.getId(), self.getSourceIdx(slot.getProcIdx()))
            #localPenalty = 1
            localPenalty = min(min(acc.getNumber(), sAccs.getNumber()), rescheduledSlot.getAccesses(addr).getNumber() - alreadyCounted)
        #alreadyCountedAway = self.interferenceEngine.getAlreadyCounted(slot.getJob().getUniqueIdx(), self.getSourceIdx(rescheduledSlot.getProcIdx()))
        alreadyCountedAway = self.interferenceEngine.getAlreadyCounted(slot.getId(), self.getSourceIdx(rescheduledSlot.getProcIdx()))
        awayPenalty = 0
        if alreadyCountedAway < slot.getAccesses(addr).getNumber():
            #self.interferenceEngine.incrementCount(slot.getJob().getUniqueIdx(), self.getSourceIdx(rescheduledSlot.getProcIdx()))
            self.interferenceEngine.incrementCount(slot.getId(), self.getSourceIdx(rescheduledSlot.getProcIdx()))
            #awayPenalty = 1
            awayPenalty = min(min(acc.getNumber(), sAccs.getNumber()), rescheduledSlot.getAccesses(addr).getNumber() - alreadyCountedAway)
        return (localPenalty * self.penalty, awayPenalty * self.penalty)
                                                
        
    def getSources(self):
        return self.sources

    def getDestination(self):
        return self.destination

    def isConcerned(self, abstractAddress):
        if self.destination == None:
            # Abstract representation of a memory bank for example
            return abstractAddress.getAddress()==self.idx
        else:
            # Real address
            return abstractAddress.isIn(self.idx, self.destination) # In distributed memory models, the memory "bank" idx is necessary

    def cleanInterferenceEngine(self, remain): # Possibly deprecated
        nIFe = self.interferenceEngine.cloneCE(remain)
        self.interferferenceEngine = nIFe

    def clone(self, remain):
        nArbiter = Arbiter(self.idx, self.penalty, self.sources, self.destination)
        nArbiter.interferenceEngine= self.interferenceEngine.cloneCE(remain)
        return nArbiter
        
########################################################################
# Instanciations of architectures for interference analysis
########################################################################

class MultiProcBus(Interconnect):
    """ A class describing a communication bus between multiple processors
    for message passing in distributed memory model (not a multicore
    bus).
A multiprocessor bus is modelled as a sequential processor, the
    transfer durations can be either budgets for each data type
    provided in a list, or can be computed using the size of the type
    and by modelling the behavior of the bus (packet size and transfer
    rate etc.)

    Message passing duration can be seen as a special case of interference by the bus ?

    To be implemented later
    """
    def __init__(self, idx, busKind, budgets=None):
        bus= Processor(idx, budgets, busKind)
        Interconnect.__init__(self, [bus])
        
    def cleanInterferenceEngines(self, remain):
        """Does nothing bus since a bus is an interconnect resource, it has
        to have this method, unless we create an intermediate class
        just for interconnect resources

        """
        ()

    def clone(self, remain):
        #TODO implement this
        return None
        
class GenericMulticoreWithBus(ArchInterference):
    """
    A special case of multicore with just one shared sequential bus to the main memory
    """
    def __init__(self, arch, bus):
        # pIdxList=[]
        # for p in arch.getProcs():
        #     pIdxList.append(p.getIdx())
        # interconnect= Interconnect([Arbiter(0, busPenalty, pIdxList)])
        ArchInterference.__init__(self, arch, bus)

    def __str__(self):
        return str(self.archi)
        
    def computeInterferenceForSlot(self, rescheduledSlot, interval, cloneProcs):
        """Computes the interferences generated by one slot and update all
        concerned slots.  For now we consider no routing decision in
        the architecture: for a given memory access, there is only one
        path from the processor to that address.
        """
        penalty = 0

        # We treat each access of the slot separately, since they may involve separate arbiters
        accessesAddr = rescheduledSlot.getAccesses()        
        for acc in accessesAddr:
            addr = acc.getAddress()
            #Step 0: find the arbiter on which there may be interference
            for arb in self.interconnect.concernedResources(addr): # At most 1 arbiter for now
                #Step 1: find interfering slots on other cores (1 max per core, by construction of our algorithm)
                for src in arb.getSources(): # src is an index
                    if src != rescheduledSlot.getProcIdx(): # We do not want to conseider the slot itself as an interference source
                        
                        srcProc = cloneProcs[src]
                        slot = srcProc.getSlot(rescheduledSlot.getStart()) # Returns the slot on srcProc active at date getStart() or None
                        
                        if slot != None:
                            #Step 2: compute additional interference for these slots, and
                            #enlarge slots : this computation must rely on a function
                            #defined for each kind of arbiter (a method defined in the
                            #arbiters) which implements the temporal behavior of the
                            #arbiters
                            localPenalty, awayPenalty = arb.computePenalty(slot, rescheduledSlot, acc)
                            self.applyAwayPenalty(srcProc, slot, awayPenalty)
                            penalty+=localPenalty
        #Step 3: Compute timing penalty for input slot and return it
        return penalty

    def buildNew(self, nProcs, nInterconnect, scheduler):# TODO: change this to allow legacy architecture classes
        nArchi = self.archi.buildNew(nProcs, scheduler)
        nArch = GenericMulticoreWithBus(nArchi, nInterconnect)
        #nArch.setScheduler(scheduler)
        return nArch



        

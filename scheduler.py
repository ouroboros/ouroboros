from data_structures import *
from tasks import *
from architectures import *
from functions import *

######################################################################
#                                                                    #
#             Ouroboros module for scheduling                        #
#             @author: T. Carle                                      #
#                                                                    #
######################################################################



#####################################################################################################
# Description of a generic scheduler
#####################################################################################################
class Scheduler():
    """
    Description of a generic scheduler
    """    
    def __init__(self, tasks, archi, functions):
        self.tasks=tasks # Must be ordered by the index of tasks
        self.archi=archi
        self.functions=functions
        self.archi.setScheduler(self)
        self.readyJobs=[]

    def __str__(self):
        return str(self.archi)

    def getArchi(self):
        return self.archi

    def getProcs(self):
        return self.archi.getProcs()

    def getProc(self, procIdx):
        return self.archi.getProcs()[procIdx]

    def getJobPreds(self, job):
        return self.tasks[job.getIdx()].getPreds()
    
    def getBudget(self, procIdx, taskIdx):
        procType = self.getProcs()[procIdx].getType()
        funIdx = self.tasks[taskIdx].getFunIdx()
        return self.functions[funIdx].getTimeProfile(procType)
    
    def setArchi(self, archi):
        self.archi = archi
        archi.setScheduler(self)
        
    def manageReadyTasks(self): # Virtual
        """
        () -> () 

        For scheduling models in which tasks have dependencies,
        this function updates the list of tasks whose predecessors
        have already been scheduled
        """
        ()

    def selectReadyTask(self): # No optimization, to be overloaded if necessary
        assert self.readyJobs != []
        t=self.readyJobs[0]
        self.readyJobs.remove(self.readyJobs[0])
        return t

    def scheduleTask(self, t): # Virtual
        """
        (Job t) -> ()

        Takes a job as input and schedules it on the architecture resources
        """
        ()

    def computeWhile(self): # Virtual
        """
        () -> bool

        Computes the continuation condition for the while loop inside the schedule function  
        """
        return False

    def addScheduledTask(self, taskIdx): # Virtual - only for dataflow
        """
        int taskIdx -> () 

        In models where it is necessary (e.g. dataflow
        scheduler) updates the list of already scheduled tasks with
        the index given in input
        """
        ()

    def schedulingHook1(self, job):
        """
        Virtual: a hook to insert optimizations in the generic scheduling loop
        """
        ()

    def schedulingHook2(self, job):
        """
        Virtual: a hook to insert optimizations in the generic scheduling loop
        """
        ()

    def schedulingHook3(self, job):
        """
        Virtual: a hook to insert optimizations in the generic scheduling loop
        """
        ()
        
    def schedule(self):
        """
        A generic scheduling loop
        """
        while(self.computeWhile()):
 
            job = self.selectReadyTask()
            self.schedulingHook1(job)
            self.scheduleTask(job)
            self.schedulingHook2(job)
            self.addScheduledTask(job.getTaskIdx())
            self.schedulingHook3(job)
            self.manageReadyTasks()

    #########################
    # ASAP scheduling of jobs
    #########################

    def schedASAPNonPreemptiveMulticore(self, proc, job, taskBudget, interconnect = None):
        """
        Schedules a job after dateMin for taskBudget time units ASAP on the core 
        in a preemptive fashion. 
        """
        return self.schedASAPMulticore(proc, job, taskBudget, False, interconnect)

    def schedASAPPreemptiveMulticore(self, proc, job, taskBudget, interconnect = None):
        """
        Schedules a job after dateMin for taskBudget time units ASAP on the core 
        in a non-preemptive fashion.
        """
        return self.schedASAPMulticore(proc, job, taskBudget, True, interconnect)
    
    def schedASAPMulticore(self, proc, job, budget, preemptive, interconnect = None, sdate = 0):
        """
        Schedules a job ASAP on the core. Input budget is a list of 
        budgets (only one element in most models, multiple elements in 
        case of TIPs model).
        """

        varBudget = budget.getBudget()
        procSched = proc.getSchedule()
        dateMin = max(job.getReadyDateMulticore(), sdate)
        interval = None
        res=[]

        for i in range(len(varBudget)):
            localVarBudget = varBudget[i]
            while(localVarBudget>0):
                # If preemptive: get the next free interval regardless of
                # the size, ELSE: get the next free interval of size
                # varTaskBudget
                if preemptive: 
                    interval=proc.getNextFreeInterval(dateMin, localVarBudget)
                    
                else:
                    interval=proc.getNextFreeIntervalWhole(dateMin, localVarBudget)
                dateMin=interval.getStart()+interval.getSize()# Necessary for preemptions and non-preemptive segment tasks
                localVarBudget-= interval.getSize()
                res.append(interval)
        return res


        
    def scheduleJobNonPreemptiveMulticore(self, readyJob):
        res=None
        # Determine which mapping minimises the end date of the job
        for p in self.archi.getProcs():
            budget = self.getBudget(p.getIdx(), readyJob.getTaskIdx())
            if(not budget.isInfinite()):
                candIntervals = self.schedASAPNonPreemptiveMulticore(p, readyJob, budget)
                if res==None or (res.getEnd()>candIntervals[-1].getEnd()):
                    res=JobSchedulingResult(candIntervals, p.getIdx())
        return res

    def scheduleJobPreemptiveMulticore(self, readyJob):
        res=None
        # Determine which mapping minimises the end date of the job
        for p in self.archi.getProcs():
            budget = self.getBudget(p.getIdx(), readyJob.getTaskIdx())
            if(not budget.isInfinite()):
                candIntervals = self.schedASAPPreemptiveMulticore(p, readyJob, budget)
                if res==None or (res.getEnd()>candIntervals[-1].getEnd()):
                    res=JobSchedulingResult(candIntervals, p.getIdx())
        return res
    
    def verifyDependencies(self): #Checking that there is no cycle in dependencies (a.k.a. task set is a DAG). Can be overloaded when dealing with synchronous model to accomodate for delays, and check also endochrony
        isDag=True
        for task in self.tasks:
            isDag= isDag and verifTaskDependencies(task, [])
        return isDag

    def createNewTimeSlot(self, start, size, job, procIdx):
        """
        Virtual
        """
        return CompTime(start, size, job, job.getTaskIdx(), procIdx)
    
    def applySchedulingDecision(self, readyJob, result):
        # Apply optimal mapping decision:
        for interval in result.getIntervals():
            # step 1: create corresponding time slot and insert it in corresponding job
            tSlot =self.createNewTimeSlot(interval.getStart(), interval.getSize(), readyJob, result.getProcIdx())
            readyJob.addTimeSlot(tSlot)
            #step 1.1: update slot description with memory profile
            funIdx = self.tasks[readyJob.getTaskIdx()].getFunIdx()
            if(self.functions[funIdx].isDefMemProfile()):
                memProfile = self.functions[funIdx].getMemProfile(self.archi.getProcs()[result.getProcIdx()].getType())
                readyJob.updateMemAccess(memProfile)
            # step 2: insert it in processor schedule and remove corresponding free time interval
            self.archi.getProcs()[result.getProcIdx()].updateSchedule(tSlot)
        # In case of non-migrating periodic task, we must make sure other jobs will be scheduled on the same processor
        if not self.tasks[readyJob.getTaskIdx()].getMigration():
            for p in self.archi.getProcs():
                if p.idx != result.getProcIdx():
                    p.budgets[readyJob.getTaskIdx()]= ExtendedInt(0,True)
            
##############################################################################################################
# Refinements of dataflow schedulers
##############################################################################################################
class DataFlowScheduler(Scheduler):
    def __init__(self, tasks, archi, funs):
        Scheduler.__init__(self, tasks, archi, funs)
        self.remainingTaskIdx=[]#Indices of tasks that still have to be scheduled AND are not ready yet    
        self.readyJobs=[] #Jobs of the tasks whose preds have been scheduled
        for i in range(len(self.tasks)):
            if self.predsReady(i):
                self.tasks[i].generateJobs(0) #these jobs are ready at date 0 and have no predecessors (None predecessor processor)
                self.readyJobs.append(self.tasks[i].getCurrentJob())
            else:
                self.remainingTaskIdx.append(i)
        
        self.scheduledTaskIdx=[] #indices of the tasks that have been scheduled
        
    #Update readyTaskIdx and remainingTaskIdx lists once a new task has been scheduled

    def predsReady(self, taskIdx):
        """
        Used only in the initialization. To be overloaded to add e.g. synchronous semantics
        """
        return self.tasks[taskIdx].getPreds()==[] # In a simple DAG: if a node has no predecessor
    
    def manageReadyTasks(self):
            for idx in self.remainingTaskIdx:
                task=self.tasks[idx]
                ready=True
                date= 0 #[]
                predProcs=[]
                jobPreds=[]
                for pred in task.getPreds():
                    if pred.getIdx() not in self.scheduledTaskIdx:# if a predecessor is not yet scheduled
                        print(str(pred)+ " not ready")
                        ready=False
                        break
                    else:
                        #date.append((self.tasks[pred.getIdx()].getCurrentJob().getEndDate(), self.tasks[pred.getIdx()].getCurrentJob().getLastProc()))
                        jobPreds.append(self.tasks[pred.getIdx()].getCurrentJob())
                if ready:# If all predecessors have been scheduled, remove task from remaining and put it in ready list with the date when all the inputs are ready
                    task.generateJobs(date)
                    task.getCurrentJob().setPreds(jobPreds)
                    self.readyJobs.append(task.getCurrentJob())
                    self.remainingTaskIdx.remove(idx)
            assert not (len(self.remainingTaskIdx)>0 and len(self.readyJobs)==0), "Topological problem: it is likely that the dataflow graph contains loops"

    def addScheduledTask(self, taskIdx):
        self.scheduledTaskIdx.append(taskIdx)
                    
    def computeWhile(self):
        return (len(self.remainingTaskIdx)+len(self.readyJobs))>0

class DataflowMonolithic(DataFlowScheduler):
    """
    Initially defined for monolithic function descriptions, but should work also for segmented function descriptions
    """
    def __init__(self, tasks, archi, funs):
        DataFlowScheduler.__init__(self, tasks, archi, funs)

    def scheduleTask(self, readyJob): # Redirection for experiments
        self.scheduleASAPNonPreemptiveMulticore(readyJob)

    def scheduleASAPNonPreemptiveMulticore(self, readyJob):
        res = self.scheduleJobNonPreemptiveMulticore(readyJob)
        self.applySchedulingDecision(readyJob, res)
            
##############################################################################################################
# Refinements of periodic task systems schedulers
##############################################################################################################
class PeriodicTaskScheduler(Scheduler):
    def __init__(self, tasks, archi, funs):
        Scheduler.__init__(self, tasks, archi, funs)
        self.readyJobs= []
        
        # Generate all jobs and sort them
        hp = determineHyperperiod(self.tasks)
        for t in self.tasks:
            t.generateJobs(hp)
            self.readyJobs += t.getJobs()
        self.sortReadyJobs()
        

    def computeWhile(self):
        return len(self.readyJobs)>0
        
    def sortReadyJobs(self): # Virtual
        """
        () -> ()

        Sorts the list of ready jobs according to a priority policy
        """
        ()
        
class RMScheduler(PeriodicTaskScheduler):
    def __init__(self, tasks, archi, funs):
        PeriodicTaskScheduler.__init__(self, tasks, archi, funs)
        
    def sortReadyJobs(self):
        self.readyJobs.sort(key=lambda x : self.tasks[x.getTaskIdx()].getPeriod())
    
    def scheduleTask(self, readyJob): # Redirection for experiments
        self.scheduleASAPPreemptiveMulticore(readyJob)

    def scheduleASAPNonPreemptiveMulticore(self, readyJob):    
        res = self.scheduleJobNonPreemptiveMulticore(readyJob)
        # Verify deadline
        if res.getEnd() > readyJob.getDeadline():
            print("Failed to schedule "+str(readyJob)+ " in time.")
            print("Job deadline: "+str(readyJob.getDeadline())+ " finishing at: "+str(res.getEnd()))
            print(str(self))
            print("Stopping...")            
            exit(0)
        self.applySchedulingDecision(readyJob, res) 
        
    def scheduleASAPPreemptiveMulticore(self, readyJob):    
        res = self.scheduleJobPreemptiveMulticore(readyJob)
        # Verify deadline
        if res.getEnd() > readyJob.getDeadline():
            print("Failed to schedule "+str(readyJob)+ " in time.")
            print("Job deadline: "+str(readyJob.getDeadline())+ " finishing at: "+str(res.getEnd()))
            print(str(self))
            print("Stopping...")            
            exit(0)
        self.applySchedulingDecision(readyJob, res)
        
class DMScheduler(RMScheduler): # DM scheduler is RM scheduler with priority on task deadlines instead of periods
    def sortReadyJobs(self):
        self.readyJobs.sort(key=lambda x : self.tasks[x.getTaskIdx()].getDeadline())

class EDFScheduler(RMScheduler): # Static EDF is RM with priority on job deadline instead of task period 
    def sortReadyJobs(self):
        self.readyJobs.sort(key=lambda x : x.getDeadline())
        
###############################################################################################################
# Collection of functions for determining the
# hyperperiod of a periodic task system
###############################################################################################################

def lcm(i1, i2):
    imax=i1
    if i1 <i2:
        imax=i2
    i=imax
    while True:
        if i % i1 == 0 and i % i2 ==0:
            return i
        i+=imax

def lcm_list(list):
    if len(list)==0:
        return None
    if len(list)==1:
        return list[0]
    else:
        return lcm(list[0], lcm_list(list[1:]))

def determineHyperperiod(tasks):
    periods = []
    for t in tasks:
        if not (t.period in periods):
            periods.append(t.period)
    return lcm_list(periods)

###############################################################################################################
# Function that verifies that task dependencies are enforced
###############################################################################################################

def verifTaskDependencies(t, alreadyFound):
    if t not in alreadyFound:
        alreadyFound.push(t)
        ok=True
        for st in t.getPreds():
            ok = ok and verifTaskDependencies(st.getIdx(), alreadyFound)
        return ok
    else:
        return False

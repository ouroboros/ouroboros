######################################################################
#                                                                    #
#             Scheduling entities for Ouroboros                      #
#             @author: T. Carle                                      #
#                                                                    #
######################################################################





from extendedInts import *
from LL import *

slotId = 0 # Necessary to get unique identifiers for time slots 

class CompTime():
    """
    Scheduled computation time representation a.k.a. reserved time slots
    """    
    def __init__(self, date, budget, job, taskIdx, procIdx, ifBudget = 0):
        self.date = date
        self.budget = budget
        self.job = job
        self.specialFun = False # for code generation with special functions e.g. DMA
        #TODO: check that it is updated when computing interference
        job.setStartDate(date)

        self.taskIdx = taskIdx
        self.procIdx = procIdx
        self.ifBudget = ifBudget # this budget grows when there is interference in the classic model
        global slotId
        self.identifier = slotId
        slotId+=1

    def setId(self, ident):
        self.identifier = ident
        
    def getId(self):
        return self.identifier
    
    def getDate(self):
        return self.date

    def getStart(self): # Alias to comply with interface for Linked list
        return self.date
    
    def getBudget(self): # Budget does not vary with interference
        return self.budget

    def getBudgetWithIF(self):
        return self.budget + self.ifBudget
    
    def increaseIFBudget(self, penalty):
        self.ifBudget += penalty

    def getIFBudget(self):
        return self.ifBudget
    
    def getJob(self):
        return self.job
    
    def getProcIdx(self):
        return self.procIdx

    def getEnd(self):
        return self.date+self.budget+self.ifBudget

    def getTaskIdx(self):
        return self.taskIdx
    
    def clone(self):
        new = CompTime(self.date, self.budget, self.job, self.taskIdx, self.procIdx, self.ifBudget)
        new.setId(self.getId())
        return new

    def getOrder(self):
        return self.date

    def getAccesses(self, addr = None):
        if addr == None:
            return self.getJob().getAccesses(self)
        else:
            for a in self.getJob().getAccesses(self):
                if a.getAddress().getAddress() == addr.getAddress():
                    return a

    def getSpecialFun(self):
        return self.specialFun
    
    def __str__(self):
        return "task: " + str(self.taskIdx) + ", job: " + str(self.job.getIdx()) + " start: " + str(self.date) + " budget: " + str(self.getBudgetWithIF())

    


class Schedule():
    def __init__(self):
        self.timeSlots = Ll(None)
        self.freeIntervals = IntervalList(None)

    def getTimeSlots(self):
        return self.timeSlots

    def getFreeIntervals(self):
        return self.freeIntervals

    def setFreeIntervals(self,newIntervals):
        self.freeIntervals=newIntervals

    def getNextFreeInterval(self, dateMin, budget):
        return self.freeIntervals.getNextFreeInterval(dateMin, budget)

    def getNextFreeIntervalWhole(self, dateMin, budget):
        return self.freeIntervals.getNextFreeIntervalWhole(dateMin, budget)

    def removeFreeInterval(self, date, size):
        self.freeIntervals.removeFreeInterval(date, size, self)

    def updateSchedule(self, timeSlot):
        self.timeSlots.insert(timeSlot)        
        self.removeFreeInterval(timeSlot.getStart(), timeSlot.getBudgetWithIF())
        
    def printFreeIntervals(self):
        fi=self.freeIntervals
        while fi!=None:
            print("\t"+str(fi))
            fi=fi.getNext()

    def cloneUpTo(self, date):
        """
        Cloning all slots with starting date < date, except for the last one, and building the corresponding free slots representation. returning the new schedule, AND the last slot that was omitted (or None if there is no such slot)
        """
        newSched = Schedule()
        ts = self.timeSlots
        if ts.getVal()==None: # First Ll element is a sentinel
            ts=ts.getNext()
        lastSlot=None
        while ts != None and ts.getStart() < date:
            if ts.getNext()==None and ts.getEnd()>date:
                lastSlot=ts.getVal()
            else:
                newSched.updateSchedule(ts.clone())
            ts = ts.getNext()
        return newSched,lastSlot
        
    def getSlotAt(self, date):
        ts = self.timeSlots
        if ts.getVal()==None: # First Ll element is a sentinel
            ts=ts.getNext()
        while ts != None and ts.getEnd() <= date:
            ts = ts.getNext()
        if ts == None:
            return ts
        elif ts.getStart() > date:
            return None
        else:
            return ts

    def getSlotBefore(self, date):
        """
        Returns the last slot to finish before date
        """
        tsPred = None
        ts = self.timeSlots
        ts=ts.getNext()
        while ts != None and ts.getVal() != None and ts.getEnd() <= date:
            tsPred = ts
            ts = ts.getNext()
        return tsPred

    def getSlotAfter(self, date):
        """
        Returns the first slot to start after date
        """
        ts = self.timeSlots
        while ts != None and ts.getVal() != None and ts.getStart() < date:
            ts = ts.getNext()
        return tsPred
    
    def getLastSlot(self):
        ts = self.timeSlots
        if ts == None:
            return None
        while ts.getNext() != None:
            ts=ts.getNext()
        return ts

    def getEndDate(self):
        ls = self.getLastSlot()
        if ls == None or ls.getVal() == None:
            return 0
        return ls.getEnd()





class JobSchedulingResult():
    def __init__(self, intervals, procIdx):
        self.intervals=intervals
        self.procIdx=procIdx

    def getIntervals(self):
        return self.intervals
    
    def getProcIdx(self):
        return self.procIdx

    def getEnd(self):
        return self.intervals[-1].getEnd()


class Interval():
    """
    Description of a free time interval
    """
    def __init__(self, start, size):
        self.start=start
        self.size=size

    def __str__(self):
        res="interval: start= "+ str(self.start)+ " size= "+str(self.size)
        return res
    
    def setStart(self, startX=None, startY=None):
        assert not startX == None, "start date must be specified"
        self.start=startX
        
    def getStart(self):
        return self.start

    def endsBefore(self, date):
        return self.getEnd() <= date
    
    def getSize(self):
        return self.size

    def getEnd(self):
        return self.start+self.size

    def getOrder(self):
        return self.start
    
    def cutFreeInterval(self, date, size, startY = None, sizeY = None):
        sDate = max(date, self.start)
        res = Interval(sDate, min(sDate+size, self.start+self.size)-sDate)
        remains=[]
        if res.getStart()>self.start:
            remains.append(Interval(self.start, res.getStart()-self.start))
        if res.getStart()+res.getSize()<self.start + self.size:
            remains.append(Interval(res.getStart()+res.getSize(), self.start + self.size-(res.getStart()+res.getSize())))
        return res, remains

    def isIntersected(self, other):
        """
        Does the other interval intersect the current interval
        """
        return other.getEnd()> self.getStart() and other.getStart()< self.getEnd()

    def intersectsList(self, iList):
        """
        Is the interval intersected by any interval in the list ?
        """
        res = False
        for i in iList:
            res = res or self.isIntersected(i)

        return res
    
class IntervalList(Ll):
    """
    Description of a list of free time intervals for each
    processor. To be inherited by faster structures with FastTrack

    """
    def __init__(self, interval=None, startY = None, sizeY = None): # We create one interval of infinite size
        if interval == None:
            interval= Interval(0, ExtendedInt(0, True))
        Ll.__init__(self, interval)

    def buildNew(self, interval):
        return IntervalList(interval=interval)
        
    def getNextFreeInterval(self, date, size, startY=None, sizeY=None): 
        currentNode=self
        while currentNode.getNext() != None and currentNode.endsBefore(date):
            currentNode=currentNode.getNext()
        res, remains=currentNode.cutFreeInterval(date, size, startY, sizeY)
        return res

    def getNextFreeIntervalWhole(self, date, size, startY=None, sizeY=None):#return the next free interval of size <= size
        currentNode=self
        while currentNode.getNext() != None and currentNode.endsBefore(date) or\
         currentNode.getSize()<size:
            currentNode=currentNode.getNext()
        res, remains=currentNode.cutFreeInterval(date, size, startY, sizeY)
        return res
    
    def removeFreeInterval(self, date, size, sched=None, startY=None, sizeY=None):
        """ 
        When a scheduling choice has been made, we update the corresponding interval list
        """        
        currentNode=self
        while currentNode.getNext() != None and currentNode.endsBefore(date):
            currentNode=currentNode.getNext()

        res, remains = currentNode.cutFreeInterval(date, size)

        if len(remains)==2:
            newNode=IntervalList(interval=remains[1])
            currentNode.insertBefore(newNode)

        if len(remains)>0:
            newNode=IntervalList(interval=remains[0])
            currentNode.insertBefore(newNode)        
        
        ptr, isHead=currentNode.remove()
        if isHead and (sched != None):
            sched.setFreeIntervals(ptr)
        return res


class I2DMem(Interval):
    """
    Intervals with 2 dimensions: time x memory_space
    """
    def __init__(self, startX, sizeX, startY, sizeY):
        self.startX = startX
        self.startY = startY
        self.sizeX = sizeX
        self.sizeY = sizeY

    def __str__(self):
        res = "interval: \n\tstartX= " + str(self.startX) + " sizeX= " + str(self.sizeX) + "\n\tstartY= " + str(self.startY) + " sizeY= " + str(self.sizeY)
        return res
    def setStart(self, startX = None, startY = None):
        if not startX == None:
            self.startX = startX
        if not startY == None:
            self.startY = startY

    def getStart(self):
        return self.startX

    def getStartY(self):
        return self.startY
    
    def endsBefore(self, date):
        return self.getEnd() <= date

    def getSizeX(self):
        return self.sizeX

    def getSize(self):
        return self.getSizeX()
    
    def getSizeY(self):
        return self.sizeY

    def getEnd(self):
        return self.startX + self.sizeX

    def getOrder(self):
        return self.startX
        
    def cutFreeInterval(self, date, size, startY, sizeY):
        """date is availability date (may be inferior to the start date of
        the current interval). However the current interval must hold
        at least size units in the X axis, and must contain [startY,
        startY+sizeY] in the Y axis.

        """
        sd = max(date, self.startX)
        res = I2DMem(sd, size, startY, sizeY)
        remains = []
        # Horizontal
        if res.getStart() > self.startX:
            remains.append(I2DMem(self.startX, res.getStart()-self.startX, self.startY, self.sizeY))
        if res.getEnd() < self.getEnd():
            remains.append(I2DMem(res.getEnd(), self.getEnd()-res.getEnd(), self.startY, self.sizeY))
        # Vertical
        if res.getStartY() > self.startY:
            remains.append(I2DMem(self.startX, self.getEnd(), self.startY, res.getStartY()-self.startY))
        if res.getStartY()+res.getSizeY() < self.startY + self.sizeY:
            remains.append(I2DMem(self.startX, self.getEnd(), res.getStartY()+res.getSizeY(), self.sizeY+self.sizeY))
        return res, remains

    def cutIntervalStrict(self, date, size, startY, sizeY):
        """In this function, we only remove the portion of [date,
        date+size]x[startY, startY+sizeY] which intersects the current
        interval, and return the maximal remaining
        intervals. Verification of intersection between intervals must
        be done at caller level.

        """
        remains = []
        # Left part
        if date > self.startX:
            remains.append(I2DMem(self.startX, date - self.startX, self.startY, self.sizeY))
        # Right part
        if date+size < self.startX + self.sizeX:
            remains.append(I2DMem(date+size, self.startX + self.sizeX, self.startY, self.sizeY))

        # Bottom part
        if startY > self.startY:
            remains.append(I2DMem(self.startX, self.getEnd(), self.startY, startY))
        # Top part
        if startY + sizeY < self.startY + self.sizeY:
            remains.append(I2DMem(self.startX, self.sizeX, startY + sizeY, self.startY + self.sizeY))

        return remains
    
    def isIn(self, other):
        """
        Returns whether or not the current interval is completely covered by another interval
        """
        return other.getStart()<=self.startX and other.getEnd()>= self.getEnd() and other.getStartY()<= self.getStartY() and other.getStartY() + other.getSizeY() >= self.getStartY() + self.getSizeY()

    def isIntersected(self, other):
        """
        Does the other interval intersect the current interval
        """
        if self.isIn(other) or other.isIn(self):
            return True
        return (other.getStart()<self.getEnd() and other.getEnd() > self.getStart()) and (other.getStartY() <= self.startY + self.endY and  other.getStartY()+other.getSizeY() >= self.startY)

    
class I2DMemList(Ll):
    def __init__(self, interval=None, startY = None, sizeY = None): # We create one interval of infinite size
        if interval == None:
            assert not startY == None and not sizeY == None, "must specify memory addresses range"
            interval= I2DMem(0, ExtendedInt(0, True), startY=startY, sizeY=sizeY)
        Ll.__init__(self, interval)
        
    def getNbPages(self):
        return self.getSizeY()
    
    def buildNew(self, interval):
        return I2DMemList(interval=interval, startY=0, sizeY=0)

    def setFreeIntervals(self, ptr, page):
        assert False, "setFreeIntervals should not be called on an I2DMemList"
    
    def removeFreeInterval(self, date, size, pages):
        """Remove all intersecting intervals from the list. For each, obtain
        the list of maximal intervals once the interval has been
        removed. Insert elements from that list, making sure that
        non-maximal elements in the initial list and in the new
        sublists get evicted.  Specified date must be the real
        starting date, after prior investigation of available
        intervals. 

        """
        startY, sizeY = pages
        # We build the result interval ex-nihilo (Its availability must have been checked prior to calling this function)
        res = I2DMem(date, size, startY, sizeY)
        currentNode=self
        remains = []
        while currentNode != None:
            
            if currentNode.isIntersected(res):
                # Adding remainders to the remains list, while maintaining the remains list as a list of maximal intervals
                local_rem = currentNode.cutIntervalStrict(date, size, startY, sizeY)
                for r in local_rem:
                    r_out = False
                    for i in range(len(remains)):
                        if remains[i].isIn(r):
                            del remains[i]
                        elif r.isIn(remains[i]):
                            r_out = True
                            break
                    if not r_out:
                        remains.append(r)

                # Sorting the remains list by increasing start date,
                # and in case of equality by size of duration
                # (guarantees infinite interval at the end). For same date and size,
                # sorts by increasing width -> to limit fragmentation, this will
                # encourage the use of smaller width intervals first
                remains= sorted(remains, key=lambda x: (x.getStart(), x.getSize(), x.getSizeY()))
                #Inserting the remains elements
                #to the main list: must ensure that the remaining
                #elements that we insert are maximal in the main list
                for i in remains:
                    iterator = self
                    insert = True
                    
                    # We consider that the lists are all sorted by increasing start date of the intervals
                    while iterator!= None and iterator.getStart() <= i.getStart():
                        if i.isIn(iterator.getVal()):
                            insert = False
                            break
                        iterator = iterator.getNext()
                    if insert:
                        # insert i before current node
                        nl = I2DMemList(i)
                        currentNode.insertAfter(nl)
                #Removing current interval from interval list
                currentNode.remove()
            currentNode=currentNode.getNext()
        return res


    def getNextFreeIntervalWhole(self, start, size, pageDescriptor):
        """Ask for an interval starting AFTER date start, of size size and
        width pages, starting at address startAddr (by default uses
        start address of candidate free intervals)

        """
        pages, startAddr = pageDescriptor
        
        currentNode = self
        
        while currentNode.getNext() != None and currentNode.getEnd() <= start:
            currentNode = currentNode.getNext()

        
        while currentNode != None:
           
            if currentNode.getSize() - (max(start, currentNode.getStart()) - currentNode.getStart()) >= size and currentNode.getSizeY() >= pages:
                
                if startAddr != None:
                    # If the start address is specified, it must be enforced
                    if currentNode.getStartY() <= startAddr < currentNode.getStartY() + currentNode.getSizeY() and currentNode.getSizeY() - (startAddr - currentNode.getStartY())>= pages:
                        # ! => CutFreeInterval for 2D intervals does not have the same semantics than for Intervals
                        res, remains = currentNode.cutFreeInterval(max(start, currentNode.getStart()), size, startAddr, pages)
                       
                        return IntervalMem(res, (res.getSizeY(), res.getStartY()))
                else:
                    # Start address is not specified, any address will do
                    # ! => CutFreeInterval for 2D intervals does not have the same semantics than for Intervals
                    res, remains = currentNode.cutFreeInterval(max(start, currentNode.getStart()), size, currentNode.getStartY(), pages)
                    
                    return IntervalMem(res, (res.getSizeY(), res.getStartY()))
            currentNode=currentNode.getNext()
            # The last interval in the list always contains an infinite interval with full width

    def getNextFreeInterval(self, start, size, pageDescriptor):
        """Ask for an interval starting AFTER date start, of maximum size
        size and width pages, starting at address startAddr (by
        default uses start address of candidate free intervals)

        """
        
        pages, startAddr = pageDescriptor
        currentNode = self
        while currentNode.getNext() != None and currentNode.getEnd() <= start:
            currentNode = currentNode.getNext()


        while currentNode != None:
            if currentNode.getSizeY() >= pages:
                if startAddr != None:
                    # If the start address is specified, it must be enforced
                    if currentNode.getStartY() <= startAddr < currentNode.getStartY() + currentNode.getSizeY() and currentNode.getSizeY() - (startAddr - currentNode.getStartY())>= pages:
                        # ! => CutFreeInterval for 2D intervals does not have the same semantics than for Intervals
                        res, remains = currentNode.cutFreeInterval(max(start, currentNode.getStart()), min(size, currentNode.getEnd()-max(start, currentNode.getStart())), startAddr, pages)
                        return (IntervalMem(res, (res.getSizeY(), res.getStartY())), remains)
                else:
                    # Start address is not specified, any address will do
                    # ! => CutFreeInterval for 2D intervals does not have the same semantics than for Intervals
                    res, remains = currentNode.cutFreeInterval(max(start, currentNode.getStart()), min(size, currentNode.getEnd()-max(start, currentNode.getStart())), currentNode.getStartY(), pages)
                    return (IntervalMem(res, (res.getSizeY(), res.getStartY())), remains)
            currentNode=currentNode.getNext()
            # The last interval in the list always contains an infinite interval with full width

    
    def getAllMaxNextFreeIntervals(self, start, size, pages):
        """Ask for all address-maximal intervals starting at the first date >= start and
        of exactly size time units

        """
        currentNode = self
        while currentNode.getNext() != None and currentNode.getEnd() <= start:
            currentNode = currentNode.getNext()

        ints = []
        stDate= None
        while currentNode != None:
            # First time we find an interval that starts after the first found date
            if stDate != None and stDate < currentNode.getStart():
                return ints
            
            if currentNode.getSize() - (max(start, currentNode.getStart()) - currentNode.getStart()) >= size and currentNode.getSizeY() >= pages:
                # First time we find a suitable interval
                if stDate == None:
                    if start >= currentNode.getStart():
                        stDate = start
                    else:
                        stDate = currentNode.getStart()
                    
                # Every time
                i,r= currentNode.cutInterval(stDate, size, currentNode.startY(), currentNode.sizeY())
                ints.append(i)
            currentNode=currentNode.getNext()
        return ints


class IntervalMem():
    """
    An intermediate representation consisting of an interval (of class Interval or I2Dmem for example) and the description of the actual memory pages that are reserved (in case the spec did not specify a fixed address range)
    """
    def __init__(self, interval, pages):
        self.interval = interval
        self.pages = pages

    def getInterval(self):
        return self.interval

    def getPages(self):
        return self.pages
    

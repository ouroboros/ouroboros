from data_structures import *

######################################################################
#                                                                    #
#             Ouroboros module for function description              #
#             @author: T. Carle                                      #
#                                                                    #
######################################################################

#######################################
# This module contains the descrption #
# of functions, which can be used for #
# code generation but are primarily   #
# used to operate a link between      #
# functional description (task system)#
# and hardware model (architectures)  #
#######################################




#TODO: integrate in hardware (no need for wcet description in hw anymore)--> tester sur interferences. A Faire pour bounded int ?


########################################################
# Basic types
########################################################


class Budget():
    """
    A description of a function monolithic budget, which can be overloaded for more complex descriptions. ATTENTION: basic scheduler modules are designed to accept budgets as lists of one or more durations (monolithic->1, segment based-> multiple).
    """
    def __init__(self, value):
        assert type(value) == ExtendedInt, 'Simple budgets must be specified as extended integers'
        self.budget = [value]

    def isInfinite(self):
        return self.budget[0].isInfinite()

    def getBudget(self):
        """
        Must return a non-empty list
        """
        return self.budget
    
class MemProfile():
    """
    A description of a monolithic function memory profile, which can be overloaded for more complex descriptions
    """
    def __init__(self, accesses):
        """
        parameter accesses must be a list with the description of each access made in the current segment (only one segment in the case of a monolithic description)
        """
        self.accesses = [accesses] # self.accesses is a list of lists
        
    def getNewSegmentAccesses(self, advance = False):
        """When a profile is complex (multiple segments), this function
        returns the access description of the current segment under
        analysis (with internal counter). The advance boolean allows
        to specify if one is to advance in the segment description (a
        segment may be cut in multiple slots in case of preemptions,
        regardless of the function model). For monolithic functions,
        there is only one segment.

        """
        return self.accesses[0]

    def getAccesses(self, idx):
        """
        idx is the index of a slot in a job's collection of slots
        """
        return self.accesses[idx]
    
class FunctionDescriptor():
    """
    A basic description of a function, including a unique index, a name 
    (the function name in the source program files), as well as a description 
    of the time profile of the function on each of the processors, and possibly 
    the memory access profiles of the task on each processor)

    Virtual : the timeProfiles and memProfiles must be defined depending on the considered model.
    """

    def __init__(self, idx, name):
        self.idx = idx
        self.name = name
        self.timeProfiles = None
        self.memProfiles = None

    def getIdx(self):
        return self.idx
    
    def getName(self):
        return self.name
    
    def getTimeProfile(self, procKind):
        assert self.timeProfiles != None
        return self.timeProfiles[procKind]

    def isDefMemProfile(self):
        return self.memProfiles != None
    
    def getMemProfile(self, procKind):
        return self.memProfiles[procKind]

class MonolithicFunction(FunctionDescriptor):
    """
    This kind of function is specified with one WCET per processor type (integer list),
    and optionally the description of accessed memory locations, per processor 
    type ((AbstractAddress list) list) (or legacy for AbstractAddress).
    """
    def __init__(self, idx, name, timeProfiles, memProfiles = None):
        FunctionDescriptor.__init__(self, idx, name)
        self.timeProfiles = []
        for t in timeProfiles:
            if t == 0:
                self.timeProfiles.append(Budget(ExtendedInt(0, True))) #infinite budget
            else:
                self.timeProfiles.append(Budget(ExtendedInt(t, False)))

        if memProfiles == None:
            self.memProfiles = memProfiles
        else:
            self.memProfiles = []
            for profile in memProfiles:
                accs = []
                for acc in profile:
                    accs.append(MemAccess(acc[0], acc[1]))
                self.memProfiles.append(MemProfile(accs))

########################################################
# For representing tasks as series of temporal segments
########################################################

class SegmentBudget(Budget):
    """
    A description of the time budgets of segments for a task modeled as a series of time segments
    """
    def __init__(self, seg_budgets):
        Budget.__init__(self, ExtendedInt(0, False))
        self.budget = seg_budgets

    
class SegmentMemProfile(MemProfile):
    """
    A description of the memory access profile for a task modeled as a series of time segments. The access profiles must be described as a list of accesses to (possibly abstract) memory locations.
    """
    def __init__(self, access_list):
        MemProfile.__init__(self, None) #TODO: test this
        self.accesses = access_list

class SegmentFunction(FunctionDescriptor):
    """
    A description of a function in the time segments model. In this model, timeProfiles is ((int list) list) (a sequence of segments duration, for each processor kind), and memProfiles is (((AbstractAddress list) list) list) (a sequence pattern of accesses, for each processor kind) (or legacy for AbstractAddress)
    """
    def __init__(self, idx, name, timeProfiles, memProfiles = None):
        FunctionDescriptor.__init__(self, idx, name)
        self.timeProfiles = [] # One time profile per processor kind
        for l in timeProfiles:
            localProfile = []
            for t in l:
                if t == 0:
                    localProfile.append(ExtendedInt(0, True)) #infinite budget
                else:
                    localProfile.append(ExtendedInt(t, False))
            self.timeProfiles.append(SegmentBudget(localProfile))

        if memProfiles == None:
            self.memProfiles = memProfiles
        else:
            self.memProfiles = []
            for profile in memProfiles:
                sProf = []
                for seg in profile:
                    s = []
                    for acc in seg:
                        s.append(MemAccess(acc[0], acc[1]))
                    sProf.append(s)
                self.memProfiles.append(SegmentMemProfile(sProf))

    def verifySpec(self):
        """
        Simple check on concordance of specified time profiles and memory profiles
        """
        assert len(self.timeProfiles) == len(self.memProfiles), "Number of processor kind descriptions does not match"
        for pk in len(self.timeProfiles):
            assert len(self.getTimeProfile(pk))==len(self.getMemProfile(pk)), "Number of segments differs for processor kind: "+ str(pk)
        
class MemAccess():
    def __init__(self, address, number):
        self.address = address
        self.number = number

    def getAddress(self):
        return self.address

    def getNumber(self):
        return self.number

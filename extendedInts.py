######################################################################
#                                                                    #
#             Extended Integers                                      #
#             @author: T. Carle                                      #
#                                                                    #
######################################################################





#Integers + infinity
class ExtendedInt():
    def __init__(self, val, inf):
        self.val=val
        self.inf=inf

    def __str__(self):
        if self.inf:
            return 'Inf'
        else:
            return str(self.val)
        
    def isInfinite(self):
        return self.inf
    
    def getVal(self):
        return self.val

    def __add__(self, other):
        if type(other)==int:
            return ExtendedInt(self.val+other, self.inf)
        
        if self.inf:
            return ExtendedInt(self.val, self.inf)
        else:
            return ExtendedInt(self.val+other.getVal(), other.isInfinite())

    def __radd__(self, other):
        return self.__add__(other)

    def __mul__(self, other):
        if type(other)==int:
            return ExtendedInt(self.val*other, self.inf)
        assert False, "For now we only multiply an extended int with an int"

    def __rmul__(self, other):
        return self.__mul__(other)
    
    def __sub__(self, other):
        if type(other)==int:
            return ExtendedInt(self.val-other, self.inf)
        # We should never substract an infinite number
        assert not other.isInfinite()
        if self.inf:
            return ExtendedInt(self.val, self.inf)
        else:
            return ExtendedInt(self.val-other.getVal(), False)

    def __rsub__(self, other):
        o = ExtendedInt(other, False)
        return o.__sub__(self)
    
    def __lt__(self, other):
        if type(other)==int:
            return (not self.inf) and self.val<other
        if self.inf:
            return False
        else:
            if other.isInfinite():
                return True
            else:
                return self.val < other.getVal()

    def __le__(self, other):
        if type(other)==int:
            return (not self.inf) and self.val<=other
        if self.inf:
            return other.isInfinite()
        else:
            if other.isInfinite():
                return True
            else:
                return self.val <= other.getVal()

    def __gt__(self, other):
        if type(other)==int:
            return self.inf or self.val>other
        if self.inf:
            return True
        else:
            if other.isInfinite():
                return False
            else:
                return self.val > other.getVal()

    def __ge__(self, other):

        if type(other)==int:
            return self.inf or self.val>=other
        
        if self.inf:
            return not other.isInfinite()
        else:
            if other.isInfinite():
                return False
            else:
                return self.val >= other.getVal()

    def __eq__(self, other):
        if type(other)==int:
            return (not self.inf) and self.val==other
        if self.inf:
            if other.isInfinite():
                return True
            else:
                return False
        else:
            if other.isInfinite():
                return False
            else:
                return self.val == other.getVal()

    def __ne__(self, other):
        if type(other)==int:
            return self.inf or not (self.val==other)
        return not self == other
